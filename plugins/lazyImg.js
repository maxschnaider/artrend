/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Vue from 'vue'
import LazyImg from '../components/LazyImg.vue'

Vue.component('LazyImg', LazyImg);
