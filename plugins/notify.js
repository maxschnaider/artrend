/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Vue from 'vue'
import VueNotifications from 'vue-notification'

Vue.use(VueNotifications);
