/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import AWSService from '../services/AWSService'

export default (ctx, inject) => {
  const aws = new AWSService(ctx);
  ctx.$aws = aws;
  inject('aws', aws);
}
