/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import ApiService from '../services/ApiService'

export default (ctx, inject) => {
  const api = new ApiService(ctx);
  ctx.$api = api;
  inject('api', api);
}
