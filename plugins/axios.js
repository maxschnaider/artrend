/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

import { cacheAdapterEnhancer } from 'axios-extensions';
import LRUCache from 'lru-cache';
const defaultCache = new LRUCache({ maxAge: 1000 * 60 * 60 });

export default function ({ $axios, redirect }) {
  const defaults = $axios.defaults;
  // defaults.headers.common['Access-Control-Allow-Origin'] = '*';

  defaults.adapter = cacheAdapterEnhancer(defaults.adapter, {
    enabledByDefault: false,
    cacheFlag: 'useCache',
    defaultCache
  });

  $axios.onRequest(config => {
    // console.log('Making request to ' + config.url)
  });

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status);
    if (code === 400) {
      redirect('/400')
    }
  });

  // $axios.onResponse(response => {
  //   console.log(response)
  // })
}
