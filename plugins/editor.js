/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as EditorService from '../services/EditorService'

export default (ctx, inject) => {
  const editor = EditorService.newEditor(ctx);
  ctx.$editor = editor;
  inject('editor', editor);
}
