/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Scene from '../models/Scene'

export const state = () => ({

  list: []

});

export const mutations = {

  add (state, sceneData) {
    let scene = sceneData instanceof Scene ? sceneData : new Scene(sceneData);
    state.list.push(scene);
  },

  set (state, scenesData) {
    let scenes = [];
    for (let sceneData of scenesData) {
      let scene = sceneData instanceof Scene ? sceneData : new Scene(sceneData);
      scenes.push(scene);
    }
    state.list = scenes;
  },

  update (state, sceneData) {
    let scene = sceneData instanceof Scene ? sceneData : new Scene(sceneData);
    state.list = state.list.map(x => x._id !== scene._id ? x : scene);
  },

  remove (state, scenes) {
    state.list = state.list.filter(x => x._id !== scenes._id);
  }

};

export const getters = {

  list (state) {
    return state.list
  }

};
