/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Project from '../models/Project'

export const state = () => ({
  commonProjectsList: [],
  userProjectsList: [],
  currentProject: {}
})

export const mutations = {
  addCommonProject(state, projectData) {
    let project = projectData instanceof Project ? projectData : new Project(projectData)
    state.commonProjectsList.push(project)
  },

  setCommonProjects(state, projectsData) {
    let projects = []
    for (let projectData of projectsData) {
      let project = projectData instanceof Project ? projectData : new Project(projectData)
      projects.push(project)
    }
    state.commonProjectsList = projects
  },

  updateCommonProject(state, projectData) {
    let project = projectData instanceof Project ? projectData : new Project(projectData)
    state.commonProjectsList = state.commonProjectsList.map(x => x._id !== project._id ? x : project)
  },

  removeCommonProject(state, project) {
    state.commonProjectsList = state.commonProjectsList.filter(x => x._id !== project._id)
  },

  addUserProject(state, projectData) {
    let project = projectData instanceof Project ? projectData : new Project(projectData)
    state.userProjectsList.push(project)
  },

  setUserProjects(state, projectsData) {
    let projects = []
    for (let projectData of projectsData) {
      let project = projectData instanceof Project ? projectData : new Project(projectData)
      projects.push(project)
    }
    state.userProjectsList = projects
  },

  updateUserProject(state, projectData) {
    let project = projectData instanceof Project ? projectData : new Project(projectData)
    state.userProjectsList = state.userProjectsList.map(x => x._id !== project._id ? x : project)
  },

  removeUserProject(state, project) {
    state.userProjectsList = state.userProjectsList.filter(x => x._id !== project._id)
  },

  setCurrentProject(state , projectData) {
    let project = projectData instanceof Project ? projectData : new Project(projectData)
    state.currentProject = project
  }
}

export const getters = {
  userProjectsList(state) {
    return state.userProjectsList
  },

  commonProjectsList(state) {
    return state.commonProjectsList
  },

  currentProject(state) {
    return state.currentProject
  }
}
