/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Environment from '../models/Environment'

export const state = () => ({

  list: []

});

export const mutations = {

  add (state, environmentData) {
    let environment = environmentData instanceof Environment ? environmentData : new Environment(environmentData);
    state.list.push(environment);
  },

  set (state, environmentsData) {
    let environments = [];
    for (let environmentData of environmentsData) {
      let environment = environmentData instanceof Environment ? environmentData : new Environment(environmentData);
      environments.push(environment);
    }
    state.list = environments;
  },

  update (state, environmentData) {
    let environment = environmentData instanceof Environment ? environmentData : new Environment(environmentData);
    state.list = state.list.map(x => x._id !== environment._id ? x : environment);
  },

  remove (state, environments) {
    state.list = state.list.filter(x => x._id !== environments._id);
  }

};

export const getters = {

  list (state) {
    return state.list
  }

};
