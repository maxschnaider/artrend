/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Team from '../models/Team'

export const state = () => ({

  list: []

});

export const mutations = {

  add (state, teamData) {
    let team = teamData instanceof Team ? teamData : new Team(teamData);
    state.list.push(team);
  },

  set (state, teamsData) {
    let teams = [];
    for (let teamData of teamsData) {
      let team = teamData instanceof Team ? teamData : new Team(teamData);
      teams.push(team);
    }
    state.list = teams;
  },

  update (state, teamData) {
    let team = teamData instanceof Team ? teamData : new Team(teamData);
    state.list = state.list.map(x => x._id !== team._id ? x : team);
  },

  remove (state, teams) {
    state.list = state.list.filter(x => x._id !== teams._id);
  }

};

export const getters = {

  list (state) {
    return state.list
  }

};
