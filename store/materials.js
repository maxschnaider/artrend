/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Material from '../models/Material'

export const state = () => ({
  list: []
})

export const mutations = {
  add(state, materialData) {
    let material = materialData instanceof Material ? materialData : new Material(materialData)
    state.list.push(material)
  },

  set(state, materialsData) {
    let materials = [];
    for (let materialData of materialsData) {
      let material = materialData instanceof Material ? materialData : new Material(materialData)
      materials.push(material)
    }
    state.list = materials
  },

  update(state, materialData) {
    let material = materialData instanceof Material ? materialData : new Material(materialData)
    state.list = state.list.map(x => x._id !== material._id ? x : material)
  },

  remove(state, materials) {
    state.list = state.list.filter(x => x._id !== materials._id)
  }
}

export const getters = {
  list(state) {
    return state.list
  }
}
