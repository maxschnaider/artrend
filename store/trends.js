/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Trend from '../models/Trend'

export const state = () => ({
  list: []
})

export const mutations = {
  add(state, trendData) {
    let trend = trendData instanceof Trend ? trendData : new Trend(trendData)
    state.list.push(trend)
  },

  set(state, trendsData) {
    let trends = []
    for (let trendData of trendsData) {
      let trend = trendData instanceof Trend ? trendData : new Trend(trendData)
      trends.push(trend)
    }
    state.list = trends
  },

  update(state, trendData) {
    let trend = trendData instanceof Trend ? trendData : new Trend(trendData)
    state.list = state.list.map(x => x._id !== trend._id ? x : trend)
  },

  remove(state, trends) {
    state.list = state.list.filter(x => x._id !== trends._id)
  }
}

export const getters = {
  list(state) {
    return state.list
  }
}
