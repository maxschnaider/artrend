/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import User from '../models/User'

export const state = () => ({
  list: []
})

export const mutations = {
  add(state, userData) {
    let user = userData instanceof User ? userData : new User(userData);
    state.list.push(user);
  },

  set(state, usersData) {
    let users = []
    for (let userData of usersData) {
      let user = userData instanceof User ? userData : new User(userData)
      users.push(user)
    }
    state.list = users
  },

  update(state, userData) {
    let user = userData instanceof User ? userData : new User(userData)
    state.list = state.list.map(x => x._id !== user._id ? x : user)
  },

  remove(state, users) {
    state.list = state.list.filter(x => x._id !== users._id)
  }
}

export const getters = {
  list(state) {
    return state.list
  }
}
