/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Surface from '../models/Surface'

export const state = () => ({

  list: []

});

export const mutations = {

  add (state, surfaceData) {
    let surface = surfaceData instanceof Surface ? surfaceData : new Surface(surfaceData);
    state.list.push(surface);
  },

  set (state, surfacesData) {
    let surfaces = [];
    for (let surfaceData of surfacesData) {
      let surface = surfaceData instanceof Surface ? surfaceData : new Surface(surfaceData);
      surfaces.push(surface);
    }
    state.list = surfaces;
  },

  update (state, surfaceData) {
    let surface = surfaceData instanceof Surface ? surfaceData : new Surface(surfaceData);
    state.list = state.list.map(x => x._id !== surface._id ? x : surface);
  },

  remove (state, surfaces) {
    state.list = state.list.filter(x => x._id !== surfaces._id);
  }

};

export const getters = {

  list (state) {
    return state.list
  }

};
