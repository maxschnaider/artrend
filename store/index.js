/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

export const state = () => ({

  loading: false,
  editor: {}

});

export const mutations = {

  setLoadingState (state, isLoading) {
    state.loading = isLoading;
  },

  setEditor (state, editor) {
    state.editor = editor;
  }
};

export const actions = {

  async nuxtServerInit ({ commit }, { app, route }) {
    console.log('============= `' + route.name + '` Route Init =============\n')
  }
};
