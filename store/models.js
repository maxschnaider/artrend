/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Model from '../models/Model'

export const state = () => ({
  list: []
})

export const mutations = {
  add(state, modelData) {
    let model = modelData instanceof Model ? modelData : new Model(modelData)
    state.list.push(model)
  },

  set(state, modelsData) {
    let models = []
    for (let modelData of modelsData) {
      let model = modelData instanceof Model ? modelData : new Model(modelData)
      models.push(model)
    }
    state.list = models
  },

  update(state, modelData) {
    let model = modelData instanceof Model ? modelData : new Model(modelData)
    state.list = state.list.map(x => x._id !== model._id ? x : model)
  },

  remove(state, model) {
    state.list = state.list.filter(x => x._id !== model._id)
  }
};

export const getters = {
  list(state) {
    return state.list
  }
}
