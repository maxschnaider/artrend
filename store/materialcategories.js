/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import MaterialCategory from '../models/MaterialCategory'

export const state = () => ({

  list: []

});

export const mutations = {

  add (state, materialCategoryData) {
    let materialCategory = materialCategoryData instanceof MaterialCategory ? materialCategoryData : new MaterialCategory(materialCategoryData);
    state.list.push(materialCategory);
  },

  set (state, materialCategoriesData) {
    let materialCategories = [];
    for (let materialCategoryData of materialCategoriesData) {
      let materialCategory = materialCategoryData instanceof MaterialCategory ? materialCategoryData : new MaterialCategory(materialCategoryData);
      materialCategories.push(materialCategory);
    }
    state.list = materialCategories;
  },

  update (state, materialCategoryData) {
    let materialCategory = materialCategoryData instanceof MaterialCategory ? materialCategoryData : new MaterialCategory(materialCategoryData);
    state.list = state.list.map(x => x._id !== materialCategory._id ? x : materialCategory);
  },

  remove (state, materialCategories) {
    state.list = state.list.filter(x => x._id !== materialCategories._id);
  }

};

export const getters = {

  list (state) {
    return state.list
  }

};
