/*
 * Project: artrend-backend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

exports.handle = fn =>
    (req, res, next) => {
        Promise.resolve(fn(req, res, next))
            .catch((e) => {
                return res.json({ error: e })
            });
    };
