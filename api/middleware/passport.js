/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const mongoose = require('mongoose');
const passport = require('passport');
const config = require('../config/config');
const LocalStrategy = require('passport-local');
const JWTStrategy = require('passport-jwt').Strategy,
    ExtractJWT = require('passport-jwt').ExtractJwt;

require('../models/User');
const User = mongoose.model('User');

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
}, (username, password, done) => {
    User.findOne({ username })
        .then((user) => {
            user.validatePassword(password, function (result) {
                if(!user || !result) {
                    return done(null, false, { errors: { 'username or password': 'is invalid' } });
                }
                return done(null, user);
            });
        }).catch(done);
}));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : config.secret
    },
    function (jwtPayload, done) {
        if (!jwtPayload._id.match(/^[0-9a-fA-F]{24}$/)) {
            return done({error: 'not valid id'});
        }
        return User.findById(jwtPayload._id)
            .then(user => {
                return done(null, user);
            })
            .catch(err => {
                return done(err);
            });
    }
));
