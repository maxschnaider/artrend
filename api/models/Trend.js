/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const mongoose = require('mongoose')

const TrendSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.ObjectId,
        auto: true
    },
    name: {
        type: String,
        required: true
    },
    thumbnailUrl: String,
    materialBySurface: [{
        surfaceId: mongoose.Types.ObjectId,
        materialId: mongoose.Types.ObjectId
    }]
})

module.exports = mongoose.model('Trend', TrendSchema)
