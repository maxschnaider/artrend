/*
 * Environment: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const mongoose = require('mongoose')

const EnvironmentSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.ObjectId,
        auto: true
    },
    name: {
        type: String,
        required: true
    },
    cubeTexture: [{
        fileUrl: String
    }],
    thumbnailUrl: String,
    accessType: {
        type: Number,
        default: 0
    },
    type: String, //evening, morning, sea
    rotation: {
        type: Number,
        default: 0.0
    },
    lights: [{
        type: String, //ambient
        position: {
            x: {
                type: Number,
                default: 0.0
            },
            y: {
                type: Number,
                default: 0.0
            },
            z: {
                type: Number,
                default: 0.0
            },
        },
        color: String,
        lookAt: {
            x: {
                type: Number,
                default: 0.0
            },
            y: {
                type: Number,
                default: 0.0
            },
            z: {
                type: Number,
                default: 0.0
            },
        },
        intensity: {
            type: Number,
            default: 0.5
        }
    }]
})

module.exports = mongoose.model('Environment', EnvironmentSchema)
