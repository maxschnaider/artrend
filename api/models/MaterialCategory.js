/*
 * Project: artrend-backend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const mongoose = require('mongoose')

const MaterialCategorySchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.ObjectId,
        auto: true
    },
    name: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('MaterialCategory', MaterialCategorySchema)
