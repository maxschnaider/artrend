/*
 * Scene: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const mongoose = require('mongoose')

const SceneSchema = mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true
  },
  modelId: mongoose.Types.ObjectId,
  trendId: mongoose.Types.ObjectId,
  environmentId: mongoose.Types.ObjectId,
  lights: [{
    type: String, //ambient, point, direct
    position: {
      x: {
        type: Number,
        default: 0.0
      },
      y: {
        type: Number,
        default: 0.0
      },
      z: {
        type: Number,
        default: 0.0
      },
    },
    color: String,
    lookAt: {
      x: {
        type: Number,
        default: 0.0
      },
      y: {
        type: Number,
        default: 0.0
      },
      z: {
        type: Number,
        default: 0.0
      },
    },
    intensity: {
      type: Number,
      default: 0.5
    }
  }],
  objects3d: [{
    objectId: mongoose.Types.ObjectId,
    position: {
      x: {
        type: Number,
        default: 0.0
      },
      y: {
        type: Number,
        default: 0.0
      },
      z: {
        type: Number,
        default: 0.0
      },
    },
    rotation: Number
  }]
})

module.exports = mongoose.model('Scene', SceneSchema)
