/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const mongoose = require('mongoose')

const SurfaceSchema = mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true
  },
  name: {
    type: String,
    required: true
  },
  thumbnailUrl: String,
  color: String
})

module.exports = mongoose.model('Surface', SurfaceSchema)
