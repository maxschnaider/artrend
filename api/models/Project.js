/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const mongoose = require('mongoose')

const ProjectSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.ObjectId,
        auto: true
    },
    name: {
        type: String,
        required: true
    },
    authorId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    thumbnailUrl: String,
    sceneId: mongoose.Types.ObjectId,
    accessType: {
        type: Number,
        default: 0
    },
    invitedUsers: [{
        userId: mongoose.Types.ObjectId,
        accessLevel: {
            type: Number,
            default: 1
        }
    }],
    createDate: {
        type: Date,
        default: Date.now
    }
})

const Project = module.exports = mongoose.model('Project', ProjectSchema)
