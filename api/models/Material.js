/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const mongoose = require('mongoose')
const Config = require('../config/config')

const MaterialSchema = mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true
  },
  name: {
    type: String,
    required: true
  },
  categoryId: mongoose.Schema.ObjectId,
  accessType: {
    type: Number,
    default: 0
  },
  thumbnailColor: String,
  fileOriginalName: String,
  filePath: String,
  fileName: String,
  fileHost: {
    type: String,
    default: Config.awsBucketUrl
  },
  qualities: [{
    name: String,
    jpegQuality: Number,
    size: Number,
    fileName: String,
  }],
  settings: {
    scaleX: {
      type: Number,
      default: 1.0
    },
    scaleY: {
      type: Number,
      default: 1.0
    },
    rotation: {
      type: Number,
      default: 0
    },
    roughness: {
      type: Number,
      default: 0.5
    },
    metalness: {
      type: Number,
      default: 0.5
    },
    color: String,
    opacity: {
      type: Number,
      default: 1
    }
  }
})

module.exports = mongoose.model('Material', MaterialSchema)
