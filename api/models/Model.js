/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const mongoose = require('mongoose')

const ModelSchema = mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true
  },
  name: {
    type: String,
    required: true
  },
  fileUrl: {
    type: String,
    required: true
  },
  fileName: {
    type: String,
    required: true
  },
  filePath: {
    type: String,
    required: true
  },
  size: {
    type: Number
  },
  surfaceBySlot: [{
    slotId: Number,
    surfaceId: mongoose.Types.ObjectId
  }],
  accessType: {
    type: Number,
    default: 0
  }
})

module.exports = mongoose.model('Model', ModelSchema)
