/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const mongoose = require('mongoose')

const TeamSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.ObjectId,
        auto: true
    },
    name: {
        type: String,
        required: true
    },
    authorId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    thumbnailUrl: String,
    members: [{
        _id: mongoose.Types.ObjectId,
        name: String,
        accessLevel: Number
    }]
})

module.exports = mongoose.model('Team', TeamSchema)
