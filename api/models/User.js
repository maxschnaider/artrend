/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('../config/config')

const UserSchema = mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  },
  fullname: String,
  phone: String,
  thumbnailUrl: String,
  projectsId: [mongoose.Schema.ObjectId],
  createDate: {
    type: Date,
    default: Date.now
  },
  accountType: {
    type: Number,
    default: 0
  }
})

UserSchema.pre('save', function (next) {
  const user = this
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) {
        return next(err)
      }
      user.password = hash
      next()
    })
  })
})

UserSchema.methods.validatePassword = function validatePassword(password, callback) {
  bcrypt.compare(password, this.password, function (err, result) {
    return callback(result)
  })
}

UserSchema.methods.generateJWT = function generateJWT() {
  return jwt.sign({
    _id: this._id,
    username: this.username,
  }, config.secret, { expiresIn: "1d" })
}

UserSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    user: {
      _id: this._id,
      username: this.username
    },
    access_token: this.generateJWT(),
    token_type: 'Bearer',
    expires_in: '1d'
  }
}

module.exports = mongoose.model('User', UserSchema)
