/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const express = require('express')
const app = express()
const dotenv = require('dotenv').config()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cors = require('cors')
const compression = require('compression')
const Config = require('./config/config')
// const fs = require('fs-extra')
// const path = require('path')

app.use(compression())
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

mongoose.promise = global.Promise
mongoose.connect(`mongodb://${Config.dbConnectionString}`, { useNewUrlParser: true, useFindAndModify: false })

app.use('/', require('./router'))

module.exports = {
  path: '/api',
  handler: app
}
