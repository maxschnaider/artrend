/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/MaterialCategory')
const MaterialCategory = require('mongoose').model('MaterialCategory')

exports.getAll = async (req, res, next) => {
  const materialCategories = await MaterialCategory.find().limit()
  return res.json({ materialCategories: materialCategories })
}

exports.getById = async (req, res, next) => {
  const materialCategory = await MaterialCategory.findById(req.params._id)
  return res.json({ materialCategory: materialCategory })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newMaterialCategory = await MaterialCategory.create(data)
  return res.json({ materialCategory: newMaterialCategory })
}

exports.update = async (req, res, next) => {
  const data = {
    name: req.body.name
  }
  const updatedMaterialCategory = await MaterialCategory.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ materialCategory: updatedMaterialCategory })
}

exports.remove = async (req, res, next) => {
  const materialCategory = await MaterialCategory.findById(req.params._id)
  await materialCategory.remove()
  return res.json({ success: true })
}
