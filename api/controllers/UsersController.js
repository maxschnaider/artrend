/*
 * Project: aruser
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

require('../models/User')
const User = require('mongoose').model('User')

exports.getAll = async (req, res, next) => {
  const users = await User.find().limit()
  return res.json({ users: users })
}

exports.getById = async (req, res, next) => {
  const user = await User.findById(req.params._id)
  return res.json({ user: user })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newUser = await User.create(data)
  return res.json({ user: newUser })
}

exports.update = async (req, res, next) => {
  const data = {
    fullname: req.body.fullname,
    phone: req.body.phone,
    thumbnailUrl: req.body.thumbnailUrl,
    projects: req.body.projects
  }
  const updatedUser = await User.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ user: updatedUser })
}

exports.remove = async (req, res, next) => {
  const user = await User.findById(req.params._id)
  await user.remove()
  return res.json({ success: true })
}
