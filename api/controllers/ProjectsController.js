/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

require('../models/Project')
const Project = require('mongoose').model('Project')

exports.getAll = async (req, res, next) => {
  const projects = await Project.find().limit()
  if (!projects) {
    return res.json({ error: 'Database error' })
  }
  return res.json({ projects: projects })
}

exports.getById = async (req, res, next) => {
  const project = await Project.findById(req.params._id)
  if (!project) {
    return res.json({ error: 'No object was found' })
  }
  return res.json({ project: project })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newProject = await Project.create(data)
  if (!newProject) {
    return res.json({ error: 'Database error' })
  }
  return res.json({ project: newProject })
}

exports.update = async (req, res, next) => {
  const data = {
    name:  req.body.name,
    thumbnailUrl: req.body.thumbnailUrl,
    invitedUsers: req.body.invitedUsers,
    accessType: req.body.accessType
  }
  const updatedProject = await Project.findByIdAndUpdate(req.params._id, data, { new: true })
  if (!updatedProject) {
    return res.json({ error: 'Database error' })
  }
  return res.json({ project: updatedProject })
}

exports.remove = async (req, res, next) => {
  const project = await Project.findById(req.params._id)
  if (!await project.remove()) {
    return res.json({ error: 'Database error' })
  }
  return res.json({ success: true })
}
