/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/Scene')
const Scene = require('mongoose').model('Scene')

exports.getAll = async (req, res, next) => {
  const scenes = await Scene.find().limit()
  return res.json({ scenes: scenes })
}

exports.getById = async (req, res, next) => {
  const scene = await Scene.findById(req.params._id)
  return res.json({ scene: scene })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newScene = await Scene.create(data)
  return res.json({ scene: newScene })

}

exports.update = async (req, res, next) => {
  const data = {
    name: req.body.name,
    sceneId: req.body.sceneId,
    trendId: req.body.trendId,
    environmentId: req.body.environmentId,
    lights: req.body.lights,
    objects3d: req.body.objects3d
  }
  const updatedScene = await Scene.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ scene: updatedScene })
}

exports.remove = async (req, res, next) => {
  const scene = await Scene.findById(req.params._id)
  await scene.remove()
  return res.json({ success: true })
}
