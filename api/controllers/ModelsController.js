/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/Model')
const Model = require('mongoose').model('Model')
const s3 = require('../services/aws/S3')

exports.getAll = async (req, res, next) => {
  const models = await Model.find().limit()
  return res.json({ models: models })
}

exports.getById = async (req, res, next) => {
  const model = await Model.findById(req.params._id)
  return res.json({ model: model })
}

exports.create = async (req, res, next) => {
  if (req.file.originalname === undefined) {
    return res.json({ error: 'No file' })
  }
  const data = req.body
  data.fileName = data.fileName || `${Date.now()}_${req.file.originalname}`
  const s3Res = await s3.upload(req.file.buffer, data.fileName, data.filePath)
  if (s3Res.error && !s3Res.fileUrl) {
    return res.json({ error: 'S3 upload fail' })
  }
  data.fileUrl = s3Res.fileUrl
  data.size = req.file.size
  const newModel = await Model.create(data)
  return res.json({ model: newModel })
}

exports.update = async (req, res, next) => {
  const data = {
    name:  req.body.name,
    surfaceBySlot: req.body.surfaceBySlot,
    accessType: req.body.accessType,
    fileUrl: req.body.fileUrl,
    fileName: req.body.fileName,
    filePath: req.body.filePath
  }
  const updatedModel = await Model.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ model: updatedModel })
}

exports.remove = async (req, res, next) => {
  const model = await Model.findById(req.params._id)
  const s3Res = await s3.remove(model.fileName, model.filePath)
  if (s3Res.error) {
    return res.json({ error: s3Res.error })
  }
  await model.remove()
  return res.json({ success: true })
}
