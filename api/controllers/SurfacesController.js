/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/Surface')
const Surface = require('mongoose').model('Surface')

exports.getAll = async (req, res, next) => {
  const surfaces = await Surface.find().limit()
  return res.json({ surfaces: surfaces })
}

exports.getById = async (req, res, next) => {
  const surface = await Surface.findById(req.params._id)
  return res.json({ surface: surface })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newSurface = await Surface.create(data)
  return res.json({ surface: newSurface })
}

exports.update = async (req, res, next) => {
  const data = {
    name: req.body.name,
    color: req.body.color
  }
  const updatedSurface = await Surface.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ surface: updatedSurface })
}

exports.remove = async (req, res, next) => {
  const surface = await Surface.findById(req.params._id)
  await surface.remove()
  return res.json({ success: true })
}
