/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/Environment')
const Environment = require('mongoose').model('Environment')

exports.getAll = async (req, res, next) => {
  const environments = await Environment.find().limit()
  return res.json({ environments: environments })
}

exports.getById = async (req, res, next) => {
  const environment = await Environment.findById(req.params._id)
  return res.json({ environment: environment })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newEnvironment = await Environment.create(data)
  return res.json({ environment: newEnvironment })
}

exports.update = async (req, res, next) => {
  const data = {
    name: req.body.name,
    thumbnailUrl: req.body.thumbnailUrl,
    accessType: req.body.accessType,
    type: req.body.type,
    rotation: req.body.rotation,
    lights: req.body.lights
  }
  const updatedEnvironment = await Environment.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ environment: updatedEnvironment })
}

exports.remove = async (req, res, next) => {
  const environment = await Environment.findById(req.params._id)
  await environment.remove()
  return res.json({ success: true })
}
