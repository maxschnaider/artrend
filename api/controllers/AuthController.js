/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')

exports.login = (req, res, next) => {
    const userData = req.body
    if (!userData.username || !userData.password) {
        return res.json({ error: "username and password are required" })
    }
    return passport.authenticate('local', {session: false}, (err, passportUser, info) => {
        if (err) {
            return res.json({ error: err })
        }
        if (passportUser) {
            const user = passportUser
            return res.json(user.toAuthJSON())
        }
        return res.status(401)
    })(req, res, next)
}

exports.currentUser = (req, res, next) => {
    return res.json({ user: req.user })
}
