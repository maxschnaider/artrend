/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/Trend')
const Trend = require('mongoose').model('Trend')

exports.getAll = async (req, res, next) => {
  const trends = await Trend.find().limit()
  return res.json({ trends: trends })
}

exports.getById = async (req, res, next) => {
  const trend = await Trend.findById(req.params._id)
  return res.json({ trend: trend })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newTrend = await Trend.create(data)
  return res.json({ trend: newTrend })
}

exports.update = async (req, res, next) => {
  const data = {
    name: req.body.name,
    materialBySurface: req.body.materialBySurface,
    thumbnailUrl: req.body.thumbnailUrl
  }
  const updatedTrend = await Trend.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ trend: updatedTrend })
}

exports.remove = async (req, res, next) => {
  const trend = await Trend.findById(req.params._id)
  await trend.remove()
  return res.json({ success: true })
}
