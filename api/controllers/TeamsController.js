/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/Team')
const Team = require('mongoose').model('Team')

exports.getAll = async (req, res, next) => {
  const teams = await Team.find().limit()
  return res.json({ teams: teams })
}

exports.getById = async (req, res, next) => {
  const team = await Team.findById(req.params._id)
  return res.json({ team: team })
}

exports.create = async (req, res, next) => {
  const data = req.body
  const newTeam = await Team.create(data)
  return res.json({ team: newTeam })
}

exports.update = async (req, res, next) => {
  const data = {
    name: req.body.name,
    thumbnailUrl: req.body.thumbnailUrl,
    members: req.body.members
  }
  const updatedTeam = await Team.findByIdAndUpdate(req.params._id, data, { new: true })
  return res.json({ team: updatedTeam })
}

exports.remove = async (req, res, next) => {
  const team = await Team.findById(req.params._id)
  await team.remove()
  return res.json({ success: true })
}
