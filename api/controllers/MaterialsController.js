/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

require('../models/Material')
const Material = require('mongoose').model('Material')
const assetsConfig = require('../config/assetsConfig')
const s3 = require('../services/aws/S3')
const lambda = require('../services/aws/Lambda')

exports.getAll = async (req, res, next) => {
  const materials = await Material.find().limit()
  return res.json({ materials: materials })
}

exports.getById = async (req, res, next) => {
  const material = await Material.findById(req.params._id)
  return res.json({ material: material })
}

exports.create = async (req, res, next) => {
  const data = req.body
  if (data.filePath && data.fileName) {
    const lambdaRes = await lambda.dominanteColorS3Image(data.filePath, data.fileName)
    if (lambdaRes.color) {
      data.thumbnailColor = lambdaRes.color
    } else {
      return res.json({ error: 'Наебнулась функция определения цвета (сообщить Максиму)' })
    }
  } else {
    data.thumbnailColor = data.color
  }
  const newMaterial = await Material.create(data)
  if (data.filePath && data.fileName) {
    resizeMaterialImage(newMaterial, true)
  }
  return res.json({ material: newMaterial })
}

exports.update = async (req, res, next) => {
  const data = {
    fileName: req.body.fileName
    // name: req.body.name,
    // categoryId: req.body.categoryId,
    // accessType: req.body.accessType,
    // qualities: req.body.qualities,
    // settings: req.body.settings
  }
  const updatedMaterial = await Material.findByIdAndUpdate(req.params._id, data, { new: true })
  resizeMaterialImage(updatedMaterial, true)
  return res.json({ success: true })
}

exports.remove = async (req, res, next) => {
  const material = await Material.findById(req.params._id)
  if (material.filePath.length > 'assets/materials/'.length) {
    await s3.removeFolder(material.filePath)
    await material.remove()
    return res.json({ success: true })
  } else {
    return res.json({ error: 'File path error' })
  }
}

async function resizeMaterialImage(material, updateDatabase) {
  const qualitiesData = []

  for (let i = 0; i < assetsConfig.materialSettings.sizes.length; i++) {
    const size = assetsConfig.materialSettings.sizes[i]

    for (let j = 0; j < assetsConfig.materialSettings.jpegQualities.length; j++) {
      const quality = assetsConfig.materialSettings.jpegQualities[j]
      const newFileName = `${size.name}_${quality}.jpg`

      const lambdaRes = await lambda.resizeS3Image(material.filePath, material.fileName, newFileName, size.px, quality)

      if (lambdaRes.success) {
        qualitiesData.push({
          name: size.name,
          jpegQuality: quality,
          size: size.px,
          fileName: newFileName,
        })
      }
    }

    if (updateDatabase) {
      await Material.findByIdAndUpdate(material._id, { qualities: qualitiesData })
    }
  }
}
