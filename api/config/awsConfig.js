/*
 * Project: artrend-backend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

module.exports = {
  lambda: {
    imageProcessingService: {
      pngToJpeg: "artrend-image-processing-service-prod-pngToJpeg",
      resizeS3Image: "artrend-image-processing-service-prod-resizeS3Image",
      dominanteColorS3Image: "artrend-image-processing-service-prod-dominanteColorS3Image"
    }
  }
}
