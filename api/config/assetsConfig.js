/*
 * Project: artrend-backend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

module.exports = {
    defaultPath: 'assets',
    tempPath: 'temp',
    modelSettings: {
        defaultPath: 'assets/models',
    },
    materialSettings: {
        defaultPath: 'assets/materials',
        jpegQualities: [95, 80, 60, 40],
        sizes: [
            {name: 'micro', px: 128},
            {name: 'mini', px: 256},
            {name: 'small', px: 512},
            {name: 'medium', px: 1024},
            {name: 'large', px: 2048},
            // {name: 'xlarge', px: 4096}
        ],
    }
};
