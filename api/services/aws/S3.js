/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const Config = require('../../config/config')
const assetsConfig = require('../../config/assetsConfig')
const AWS = require('aws-sdk')
const S3 = new AWS.S3()

exports.upload = function (buffer, fileName, dir) {
  return new Promise(async (resolve, reject) => {
    if (!buffer && !fileName) {
      reject("No file data")
    }
    if (!dir || dir === '') {
      dir = assetsConfig.tempPath
    }

    const params = {
      ACL: 'public-read',
      Bucket: Config.awsBucketName,
      Key: `${dir}/${fileName}`,
      Body: buffer
    }

    const uploadRes = await S3.upload(params).promise()
    if (!uploadRes.Location) {
      reject("S3 response not containes file URL")
    }
    resolve({ fileUrl: uploadRes.Location })
  })
}

exports.get = function (key) {
  new Promise((resolve, reject) => {
    const params = {
      Bucket: Config.awsBucketName,
      Key: key
    }

    S3.getObject(params, (err, data) => {
      if (err) {
        reject(err)
      }
      resolve({
        fileData: data.Body,
        contentType: data.ContentType
      })
    })
  })
}

exports.remove = function (fileName, dir) {
  return new Promise(async (resolve, reject) => {
    const params = {Bucket: Config.awsBucketName, Key: `${dir}/${fileName}`}
    let deleteRes = await S3.deleteObject(params).promise()
    if (deleteRes.$response.error) {
      reject(deleteRes.$response.error)
    }
    resolve({ success: true })
  })
}

exports.removeFolder = function (dir) {
  return new Promise(async (resolve, reject) => {
    const listParams = {
      Bucket: Config.awsBucketName,
      Prefix: dir
    }

    const listedObjects = await S3.listObjectsV2(listParams).promise()
    if (listedObjects.Contents.length === 0) {
      resolve({ success: true })
    }

    const deleteParams = {
      Bucket: Config.awsBucketName,
      Delete: { Objects: [] }
    }
    listedObjects.Contents.forEach(({ Key }) => {
      deleteParams.Delete.Objects.push({ Key })
    })

    let deleteRes = await S3.deleteObjects(deleteParams).promise()
    if (listedObjects.IsTruncated) {
      let recursionDeleteRes = await this.removeFolder(dir)
      if (recursionDeleteRes.error) {
        reject(recursionDeleteRes.$response.error)
      }
    }
    if (deleteRes.$response.error) {
      reject(deleteRes.$response.error)
    }
    resolve({ success: true })
  })
}
