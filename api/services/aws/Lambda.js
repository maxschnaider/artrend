/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const awsConfig = require('../../config/awsConfig')
const AWS = require('aws-sdk')
const lambda = new AWS.Lambda()

exports.pngToJpeg = function (dir, fileName) {
  return new Promise((resolve, reject) => {
    let lambdaParams = {
      FunctionName : awsConfig.lambda.imageProcessingService.pngToJpeg,
      InvocationType : 'RequestResponse',
      LogType : 'None',
      Payload: JSON.stringify({
        dir: dir,
        fileName: fileName
      })
    };

    lambda.invoke(lambdaParams, (err, data) => {
      if (err) {
        resolve(err)
      }
      if (data === null || data.Payload === null || JSON.parse(data.Payload).success !== true) {
        resolve("Lambda returns no data")
      }
      resolve({ success: true })
    })
  })
}

exports.resizeS3Image = function (dir, originalFileName, fileName, size, quality) {
  return new Promise((resolve, reject) => {
    let lambdaParams = {
      FunctionName : awsConfig.lambda.imageProcessingService.resizeS3Image,
      InvocationType : 'RequestResponse',
      LogType : 'None',
      Payload: JSON.stringify({
        dir: dir,
        originalFileName: originalFileName,
        fileName: fileName,
        size: size,
        quality: quality
      })
    };

    lambda.invoke(lambdaParams, (err, data) => {
      if (err) {
        resolve(err)
      }
      if (data === null || data.Payload === null || JSON.parse(data.Payload).success !== true) {
        resolve("Lambda returns no data")
      }
      resolve({ success: true })
    })
  })
}

exports.dominanteColorS3Image = function (dir, fileName) {
  return new Promise((resolve, reject) => {
    let lambdaParams = {
      FunctionName : awsConfig.lambda.imageProcessingService.dominanteColorS3Image,
      InvocationType : 'RequestResponse',
      LogType : 'None',
      Payload: JSON.stringify({
        dir: dir,
        fileName: fileName
      })
    };

    lambda.invoke(lambdaParams, (err, data) => {
      if (err) {
        resolve(err)
      }
      if (data === null || data.Payload === null || JSON.parse(data.Payload).color === undefined) {
        resolve("Lambda returns no data")
      }
      resolve({ color: JSON.parse(data.Payload).color })
    })
  })
}
