// /*
//  * Project: artrend-backend
//  * Author: Max Schnaider / @maxschnaider
//  * Copyright (c) 2019.
//  */
//
// const {Storage} = require('@google-cloud/storage');
// const storage = new Storage();
// const bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET);
//
// exports.upload = async function (req, fileBuffer) {
//     if (!req.file && !fileBuffer) {
//         return;
//     }
//
//     return new Promise((resolve, reject) => {
//         const fileName = !req.body.fileName ? `${Date.now()}_${req.file.originalname}` : req.body.fileName;
//         const filePath = !req.body.filePath ? '' : req.body.filePath + '/';
//         const file = bucket.file(filePath + fileName);
//
//         const stream = file.createWriteStream({
//             resumable: false
//         });
//
//         stream.on('error', err => reject(err));
//
//         stream.on('finish', () => resolve({
//             fileName: fileName,
//             fileUrl: `https://www.googleapis.com/storage/v1/b/${bucket.name}/o/${encodeURIComponent(file.name)}?alt=media`
//         }));
//
//         stream.end(fileBuffer || req.file.buffer);
//     });
// };
//
// exports.remove = async function (data) {
//     return await bucket.file(`${data.filePath}/${data.fileName}`).delete();
// };
