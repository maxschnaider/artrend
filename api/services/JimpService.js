/*
 * Project: artrend-backend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const Jimp = require('jimp');
const ColorThief = require('color-thief-jimp');
const rgbHex = require('rgb-hex');

exports.processImageToJpegBuffer = async function (buffer, size, quality) {
    const image = await Jimp.read(buffer);
    if (image.bitmap.width < size && image.bitmap.height < size) {
        return
    }

    await image.quality(quality);

    if (image.bitmap.width > image.bitmap.height) {
        await image.resize(size, Jimp.AUTO);
    } else {
        await image.resize(Jimp.AUTO, size);
    }

    return image.getBufferAsync(Jimp.MIME_JPEG)
};

exports.getDominanteColor = function (buffer) {
    return new Promise(async (resolve, reject) => {
        const image = await Jimp.read(buffer);
        const dominanteColorRGB = ColorThief.getColor(image);
        resolve("#" + rgbHex(dominanteColorRGB[0], dominanteColorRGB[1], dominanteColorRGB[2]));
    })
};
