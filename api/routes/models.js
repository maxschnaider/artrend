/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const Multer = require('multer')
const multer = Multer({ storage: Multer.memoryStorage() })
const ModelsController = require('../controllers/ModelsController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ModelsController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ModelsController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), multer.single('file'), asyncHandler.handle(ModelsController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ModelsController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ModelsController.remove))

module.exports = router
