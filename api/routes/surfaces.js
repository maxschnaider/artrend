/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const Multer = require('multer')
const multer = Multer({ storage: Multer.memoryStorage() })
const SurfacesController = require('../controllers/SurfacesController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(SurfacesController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(SurfacesController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), multer.single('file'), asyncHandler.handle(SurfacesController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(SurfacesController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(SurfacesController.remove))

module.exports = router
