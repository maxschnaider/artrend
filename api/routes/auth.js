/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const AuthController = require('../controllers/AuthController')

router.post('/login', AuthController.login)
router.get('/user', passport.authenticate('jwt', { session: false }), AuthController.currentUser)

module.exports = router
