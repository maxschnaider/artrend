/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const MaterialCategoriesController = require('../controllers/MaterialCategoriesController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialCategoriesController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialCategoriesController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialCategoriesController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialCategoriesController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialCategoriesController.remove))

module.exports = router
