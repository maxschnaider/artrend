/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const Multer = require('multer')
const multer = Multer({ storage: Multer.memoryStorage() })
const EnvironmentsController = require('../controllers/EnvironmentsController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(EnvironmentsController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(EnvironmentsController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), multer.single('file'), asyncHandler.handle(EnvironmentsController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(EnvironmentsController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(EnvironmentsController.remove))

module.exports = router
