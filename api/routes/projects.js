/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const Multer = require('multer')
const multer = Multer({ storage: Multer.memoryStorage() })
const ProjectsController = require('../controllers/ProjectsController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ProjectsController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ProjectsController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ProjectsController.create))
// router.post('/', passport.authenticate('jwt', { session: false }), multer.single('file'), asyncHandler.handle(ProjectsController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ProjectsController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ProjectsController.remove))

module.exports = router
