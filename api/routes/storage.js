/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const s3 = require('../services/aws/S3')
const Multer = require('multer')
const multer = Multer({ storage: Multer.memoryStorage() })

router.post('/upload', passport.authenticate('jwt', { session: false }), multer.single('file'), asyncHandler.handle(async (req, res, next) => {
    const fileName = req.body.fileName || `${Date.now()}_${req.file.originalname}`
    const s3Res = await s3.upload(req.file.buffer, fileName, req.body.filePath)
    if (s3Res.error) {
        return res.json({ error: s3Res.error })
    }
    return res.json({ fileName: fileName, fileUrl: s3Res.fileUrl })
}))

router.post('/remove', passport.authenticate('jwt', { session: false }), asyncHandler.handle(async (req, res, next) => {
    const s3Res = await s3.remove(req.body.fileName, req.body.filePath)
    if (s3Res.error) {
        return res.json({ error: s3Res.error })
    }
    return res.json({ success: true })
}))

module.exports = router
