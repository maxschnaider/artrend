/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const TrendsController = require('../controllers/TrendsController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TrendsController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TrendsController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TrendsController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TrendsController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TrendsController.remove))

module.exports = router
