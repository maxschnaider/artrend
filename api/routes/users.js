/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const passport = require('passport');
const router = require('express').Router();
const asyncHandler = require('../middleware/asyncHandler');
const UsersController = require('../controllers/UsersController');

router.get('/', UsersController.getAll);
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(UsersController.getById));
router.post('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(UsersController.create));
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(UsersController.update));
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(UsersController.remove));

module.exports = router;
