/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const MaterialsController = require('../controllers/MaterialsController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialsController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialsController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialsController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialsController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(MaterialsController.remove))

module.exports = router
