/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const passport = require('passport')
const router = require('express').Router()
const asyncHandler = require('../middleware/asyncHandler')
const TeamsController = require('../controllers/TeamsController')

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TeamsController.getAll))
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TeamsController.getById))
router.post('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TeamsController.create))
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TeamsController.update))
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(TeamsController.remove))

module.exports = router
