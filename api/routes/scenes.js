/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const asyncHandler = require('../middleware/asyncHandler');

require('../models/Scene');
const Scene = mongoose.model('Scene');
const ScenesController = require('../controllers/ScenesController');

router.get('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ScenesController.getAll));
router.get('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ScenesController.getById));
router.post('/', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ScenesController.create));
router.put('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ScenesController.update));
router.delete('/:_id', passport.authenticate('jwt', { session: false }), asyncHandler.handle(ScenesController.remove));

module.exports = router;
