/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2018.
 */

const express = require('express')
const router = express.Router()

require('../middleware/passport')

router.use('/auth', require('../routes/auth'))
router.use('/users', require('../routes/users'))
router.use('/projects', require('../routes/projects'))
router.use('/teams', require('../routes/teams'))
router.use('/models', require('../routes/models'))
router.use('/surfaces', require('../routes/surfaces'))
router.use('/materials', require('../routes/materials'))
router.use('/materialcategories', require('../routes/materialcategories'))
router.use('/scenes', require('../routes/scenes'))
router.use('/environments', require('../routes/environments'))
router.use('/trends', require('../routes/trends'))
router.use('/storage', require('../routes/storage'))

module.exports = router
