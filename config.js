/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

const apiConfig = require('./api/config/config')

module.exports = Config = {
  title: "artrend",
  meta: [
    { charset: "utf-8" },
    { name: "viewport", content: "width=device-width, initial-scale=1" },
    { hid: "description", name: "description", content: "artrend - render a view of future" }
  ],
  link: [
    { rel: "shortcut favicon", type: "image/png", href: "../img/favicon/favicon-32x32.png" },
  ],
  // API
  // apiBaseUrl: "http://localhost:3000/api/",
  apiBaseUrl: "http://artrend.maxschnaider.com/api/",
  // endpoints
  authEndpoint: "auth/",
  usersEndpoint: "users/",
  projectsEndpoint: "projects/",
  teamsEndpoint: "teams/",
  modelsEndpoint: "models/",
  surfacesEndpoint: "surfaces/",
  materialsEndpoint: "materials/",
  materialCategoriesEndpoint: "materialcategories/",
  scenesEndpoint: "scenes/",
  environmentsEndpoint: "environments/",
  trendsEndpoint: "trends/",
  // auth
  loginEndpoint: "auth/login",
  logoutEndpoint: "auth/logout",
  authUserEndpoint: "auth/user",
  tokenType: "Bearer",
  tokenPropertyName: "access_token",
  userPropertyName: "user",
  loginUrl: "/login",
  logoutUrl: "/logout",
  callbackUrl: "/login",
  homeUrl: "/projects",
  // PWA
  themeColor: "#fff",
  backgroundColor: "#fff",
  icons: [
    {"src":"../img/favicon/android-chrome-192x192.png","sizes":"192x192","type":"image/png"},
    {"src":"../img/favicon/android-chrome-512x512.png","sizes":"512x512","type":"image/png"}
  ],
  // Google API
  googleAnalyticsID: "UA-130592674-1",
  googleTagManagerId: "GTM-5J2M264",
  // FontAwesome Icons
  solidIcons: [
    //editor
    "faLayerGroup",
    "faWarehouse",
    "faTree",
    "faMagic",
    "faPencilRuler",
    //interface
    "faChevronLeft",
    "faCloudUploadAlt",
  ],
  regularIcons: [
    // "faEye",
  ],
}
