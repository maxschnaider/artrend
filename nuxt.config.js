let Config = require('./config')
let apiConfig = require('./api/config/config')

module.exports = {
  head: {
    title: Config.title,
    meta: Config.meta,
    link: Config.link
  },
  serverMiddleware: ['~/api/index.js'],
  server: {
    port: apiConfig.port
  },
  loading: '~/components/loading.vue',
  plugins: [
    '~/plugins/axios',
    '~/plugins/api',
    '~/plugins/editor',
    '~/plugins/lazyImg',
    { src: '~/plugins/notify', ssr: false },
  ],
  build: {
    postcss: {
      plugins: {
        'postcss-preset-env': { features: { customProperties: false } },
        'cssnano': { preset: 'default' }
      },
    },
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    extractCSS: true
  },
  css: [
    { src: '~/assets/scss/main.scss', lang: 'scss' }
  ],
  router: {
    middleware: ['auth'],
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    },
    base: `/`
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: Config.loginEndpoint, method: 'post', propertyName: Config.tokenPropertyName },
          logout: { url: Config.logoutEndpoint, method: 'post' },
          user: { url: Config.authUserEndpoint, method: 'get', propertyName: Config.userPropertyName }
        },
        tokenRequired: true,
        tokenType: Config.tokenType,
      }
    },
    redirect: {
      login: Config.loginUrl,
      logout: Config.logoutUrl,
      callback: Config.callbackUrl,
      home: Config.homeUrl
    }
  },
  manifest: {
    theme_color: Config.themeColor,
    background_color: Config.backgroundColor,
    nativeUI: true,
    icons: Config.icons,
  },
  modules: [
    '@nuxtjs/auth',
    ['@nuxtjs/axios', {
      baseURL: Config.apiBaseUrl,
      https: false
    }],
    ['@nuxtjs/google-analytics', {
      id: Config.googleAnalyticsID
    }],
    // ['@nuxtjs/google-tag-manager', {
    //   id: Config.googleTagManagerId
    // }],
    '@nuxtjs/pwa',
    '@nuxtjs/bulma',
    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: Config.solidIcons
        },
        {
          set: '@fortawesome/free-regular-svg-icons',
          icons: Config.regularIcons
        }
      ]
    }],
    // '@nuxtjs/apollo',
  ],
  // apollo: {
  //   tokenName: 'artrendToken', // optional, default: apollo-token
  //   tokenExpires: 10, // optional, default: 7 (days)
  //   includeNodeModules: true, // optional, default: false (this includes graphql-tag for node_modules folder)
  //   // authenticationType: 'Basic', // optional, default: 'Bearer'
  //   // optional
  //   errorHandler (error) {
  //     console.log('%cError', 'background: red color: white padding: 2px 4px border-radius: 3px font-weight: bold', error.message)
  //   },
  //   // required
  //   clientConfigs: {
  //     default: {
  //       // required
  //       httpEndpoint: 'https://api.artrend.maxschnaider.com/api/',
  //       // optional
  //       // See https://www.apollographql.com/docs/link/links/http.html#options
  //       httpLinkOptions: {
  //         credentials: 'same-origin'
  //       },
  //       // You can use `wss` for secure connection (recommended in production)
  //       // Use `null` to disable subscriptions
  //       // wsEndpoint: 'ws://localhost:4000', // optional
  //       // LocalStorage token
  //       tokenName: 'artrendToken', // optional
  //       // Enable Automatic Query persisting with Apollo Engine
  //       persisting: false, // Optional
  //       // Use websockets for everything (no HTTP)
  //       // You need to pass a `wsEndpoint` for this to work
  //       websocketsOnly: false // Optional
  //     },
  //     // test: {
  //     //   httpEndpoint: 'http://localhost:5000',
  //     //   wsEndpoint: 'ws://localhost:5000',
  //     //   tokenName: 'apollo-token'
  //     // },
  //     // alternative: user path to config which returns exact same config options
  //     // test2: '~/plugins/my-alternative-apollo-config.js'
  //   }
  // }
}
