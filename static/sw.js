importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/009aef164209412214ba.js",
    "revision": "b0997da1e228a485d5e5a2427ea42fde"
  },
  {
    "url": "/_nuxt/0e4338761429b4eb16ac.css",
    "revision": "d41d8cd98f00b204e9800998ecf8427e"
  },
  {
    "url": "/_nuxt/10b8cdcfdce36881aaac.js",
    "revision": "ffe0053afb006ab06a981e359511ec2b"
  },
  {
    "url": "/_nuxt/16be71386de28261ac1d.js",
    "revision": "cd5b89922373018f5b4bb709cb17b63d"
  },
  {
    "url": "/_nuxt/25211c09d634e2a6fc96.js",
    "revision": "139ca5a660b5639d1d8e6774ce0cba93"
  },
  {
    "url": "/_nuxt/466312eefaeb9e8af47c.js",
    "revision": "c2865debac8b0d28fa66069281dd9b18"
  },
  {
    "url": "/_nuxt/4a642e62f212a421b7ac.js",
    "revision": "ccaba8301f8487cb8d1cf2bf881c6de4"
  },
  {
    "url": "/_nuxt/6bb0427ef9e48d2b9dc1.js",
    "revision": "48a2e8c308984668f404b7481614831d"
  },
  {
    "url": "/_nuxt/7032aeafac2bbd472a91.js",
    "revision": "95e67f9161819d8c0346a583eae8d2f0"
  },
  {
    "url": "/_nuxt/72a639143db7b03bcaf7.js",
    "revision": "b5f6fc8b3682b7f7ed35e51f17e21d55"
  },
  {
    "url": "/_nuxt/7491aaffc10bdb25d842.js",
    "revision": "e0280de803b578876c47509277bddbcc"
  },
  {
    "url": "/_nuxt/8b5fefc0d2e9692ef7af.css",
    "revision": "d41d8cd98f00b204e9800998ecf8427e"
  },
  {
    "url": "/_nuxt/974875497572e86f6d59.js",
    "revision": "b977e25f8f8f1d98e96e2a0c7b01bbe4"
  },
  {
    "url": "/_nuxt/9b2e9e29609ef2247587.css",
    "revision": "d30e1a3f33a6a2f5d6ddc0d42a5eca3f"
  },
  {
    "url": "/_nuxt/bd103a0be926f21370c9.css",
    "revision": "1a571c933eb82e09a5939cdbe9925d0e"
  },
  {
    "url": "/_nuxt/be2cbf986c0805b84bf6.js",
    "revision": "82700e44193a5c3da23af1fca7b247c8"
  },
  {
    "url": "/_nuxt/ca151c08f831e68a0f5f.js",
    "revision": "b164d6004e08642c3c128cb7d31ee93d"
  },
  {
    "url": "/_nuxt/d41dac45a5b56e98eb34.js",
    "revision": "3ce1baf77cf2803476df1f0aca47518f"
  },
  {
    "url": "/_nuxt/d60b9581605ffd865856.js",
    "revision": "ccaa9516ec65bf89a6f2e7b0fd3b64f5"
  },
  {
    "url": "/_nuxt/ded2bce603cc148aa19f.js",
    "revision": "562073ae960affa2a8d1c782a113bdc3"
  },
  {
    "url": "/_nuxt/e469cb7548466e3a1341.js",
    "revision": "1469772885dcc89c3a4fa0b76c75127a"
  }
], {
  "cacheId": "artrend",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
