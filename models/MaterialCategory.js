/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class MaterialCategory {

  constructor(_id, name) {
    if (arguments.length === 1) {
      let data = _id;
      this._id = data._id;
      this.name = data.name;

      return
    }

    this._id = _id;
    this.name = name;
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newMaterialCategory = {}
      if (this._id) {
        newMaterialCategory = await ctx.$api.materialCategories.update(this)
      } else {
        newMaterialCategory = await ctx.$api.materialCategories.create(this)
      }
      if (newMaterialCategory instanceof MaterialCategory) {
        this._id = newMaterialCategory._id
        this.name = newMaterialCategory.name;
        resolve({
          success: true,
          materialCategory: this
        })
      } else {
        resolve({ error: newMaterialCategory.error || 'API return not MaterialCategory obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.materialCategories.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'MaterialCategory have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
