/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'

export default class Model {

  constructor(_id, name, fileUrl, fileName, filePath, surfaceBySlot, accessType) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.name = data.name
      this.fileUrl = data.fileUrl
      this.fileName = data.fileName
      this.filePath = data.filePath
      this.surfaceBySlot = data.surfaceBySlot
      this.accessType = data.accessType

      return
    }

    this._id = _id
    this.name = name
    this.fileUrl = fileUrl
    this.fileName = fileName
    this.filePath = filePath
    this.surfaceBySlot = surfaceBySlot
    this.accessType = accessType
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newModel = {}
      if (this._id) {
        newModel = await ctx.$api.models.update(this)
      } else {
        newModel = await ctx.$api.models.create(this)
      }
      if (newModel instanceof Model) {
        this._id = newModel._id
        this.name = newModel.name
        this.fileUrl = newModel.fileUrl
        this.fileName = newModel.fileName
        this.filePath = newModel.filePath
        this.surfaceBySlot = newModel.surfaceBySlot
        this.accessType = newModel.accessType
        resolve({
          success: true,
          model: this
        })
      } else {
        resolve({ error: newModel.error || 'API return not Model obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.models.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Model have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }

  // setSurfaceBySlot(ctx, surfaceBySlot) {
  //   return new Promise(async resolve => {
  //     if (this._id) {
  //       resolve({ error: 'Model have no ID' })
  //     }
  //
  //     let updatedModelData = JSON.parse(JSON.stringify(this))
  //     updatedModelData.surfaceBySlot = surfaceBySlot
  //     ctx.app.store.commit(Config.modelsEndpoint + 'update', updatedModelData)
  //     resolve({ success: true })
  //   })
  // }
}
