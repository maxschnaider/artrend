/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class Trend {

  constructor(_id, name, thumbnailUrl, materialBySurface) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.name = data.name
      this.thumbnailUrl = data.thumbnailUrl
      this.materialBySurface = data.materialBySurface

      return
    }

    this._id = _id
    this.name = name
    this.thumbnailUrl = thumbnailUrl
    this.materialBySurface = materialBySurface
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newTrend = {}
      if (this._id) {
        newTrend = await ctx.$api.trends.update(this)
      } else {
        newTrend = await ctx.$api.trends.create(this)
      }
      if (newTrend instanceof Trend) {
        this._id = newTrend._id
        this.name = newTrend.name
        this.thumbnailUrl = newTrend.thumbnailUrl
        this.materialBySurface = newTrend.materialBySurface
        resolve({
          success: true,
          trend: this
        })
      } else {
        resolve({ error: newTrend.error || 'API return not Trend obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.trends.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Trend have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
