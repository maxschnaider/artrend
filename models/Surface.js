/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class Surface {

  constructor(_id, name, thumbnailUrl, color) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.name = data.name
      this.thumbnailUrl = data.thumbnailUrl
      this.color = data.color

      return
    }

    this._id = _id
    this.name = name
    this.thumbnailUrl = thumbnailUrl
    this.color = color
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newSurface = {}
      if (this._id) {
        newSurface = await ctx.$api.surfaces.update(this)
      } else {
        newSurface = await ctx.$api.surfaces.create(this)
      }
      if (newSurface instanceof Surface) {
        this._id = newSurface._id
        this.name = newSurface.name
        this.thumbnailUrl = newSurface.thumbnailUrl
        this.color = newSurface.color
        resolve({
          success: true,
          surface: this
        })
      } else {
        resolve({ error: newSurface.error || 'API return not Surface obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.surfaces.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Surface have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
