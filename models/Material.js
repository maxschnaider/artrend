/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

let qualitySettings = {
  jpegQualities: [95, 80, 60, 40],
  sizes: [
    {name: 'micro', px: 128},
    {name: 'mini', px: 256},
    {name: 'small', px: 512},
    {name: 'medium', px: 1024},
    {name: 'large', px: 2048}
  ],
}

export default class Material {

  constructor(_id, name, thumbnailColor, filePath, fileHost, qualities, settings) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.name = data.name
      this.thumbnailColor = data.thumbnailColor
      this.filePath = data.filePath
      this.fileHost = data.fileHost
      this.qualities = data.qualities
      this.settings = data.settings

      return
    }

    this._id = _id
    this.name = name
    this.thumbnailColor = thumbnailColor
    this.filePath = filePath
    this.fileHost = fileHost
    this.qualities = qualities
    this.settings = settings
  }

  fileUrl(sizeName, jpegQuality) {
    for (let quality of this.qualities) {
      if (quality.name === sizeName && quality.jpegQuality === jpegQuality) {
        return `${this.fileHost}/${this.filePath}/${quality.fileName}`
      }
    }
    for (let i = 0; i < qualitySettings.sizes.length; i++) {
      // for (let j = 0; j < qualitySettings.jpegQualities.length; j++) {
      //   if (qualitySettings.jpegQualities[j] === jpegQuality && j > 0) {
      //     let smallerJpegQuality = qualitySettings.jpegQualities[j-1]
      //     return this.fileUrl(sizeName, smallerJpegQuality)
      //   }
      // }
      if (qualitySettings.sizes[i].name === sizeName && i > 0) {
        let smallerSize = qualitySettings.sizes[i-1]
        return this.fileUrl(smallerSize.name, jpegQuality)
      }
    }
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newMaterial = {}
      if (this._id) {
        newMaterial = await ctx.$api.materials.update(this)
      } else {
        newMaterial = await ctx.$api.materials.create(this)
      }
      if (newMaterial instanceof Material) {
        this._id = newMaterial._id
        this.name = newMaterial.name
        this.thumbnailColor = newMaterial.thumbnailColor
        this.filePath = newMaterial.filePath
        this.fileHost = newMaterial.fileHost
        this.qualities = newMaterial.qualities
        this.settings = newMaterial.settings
        resolve({
          success: true,
          material: this
        })
      } else {
        resolve({ error: newMaterial.error || 'API return not Material obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.materials.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Material have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
