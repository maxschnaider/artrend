/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class Scene {

  constructor(_id, modelId, trendId, environmentId, lights, objects3d) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.modelId = data.modelId
      this.trendId = data.trendId
      this.environmentId = data.environmentId
      this.lights = data.lights
      this.objects3d = data.objects3d

      return
    }

    this._id = _id
    this.modelId = modelId
    this.trendId = trendId
    this.environmentId = environmentId
    this.lights = lights
    this.objects3d = objects3d
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newScene = {}
      if (this._id) {
        newScene = await ctx.$api.scenes.update(this)
      } else {
        newScene = await ctx.$api.scenes.create(this)
      }
      if (newScene instanceof Scene) {
        this._id = newScene._id
        this.modelId = newScene.modelId
        this.trendId = newScene.trendId
        this.environmentId = newScene.environmentId
        this.lights = newScene.lights
        this.objects3d = newScene.objects3d
        resolve({
          success: true,
          scene: this
        })
      } else {
        resolve({ error: newScene.error || 'API return not Scene obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.scenes.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Scene have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
