/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class Team {

  constructor(_id, name, authorId, thumbnailUrl, members) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.name = data.name
      this.authorId = data.authorId
      this.thumbnailUrl = data.thumbnailUrl
      this.members = data.members

      return
    }

    this._id = _id
    this.name = name
    this.authorId = authorId
    this.thumbnailUrl = thumbnailUrl
    this.members = members
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newTeam = {}
      if (this._id) {
        newTeam = await ctx.$api.teams.update(this)
      } else {
        newTeam = await ctx.$api.teams.create(this)
      }
      if (newTeam instanceof Team) {
        this._id = newTeam._id
        this.name = newTeam.name
        this.authorId = newTeam.authorId
        this.thumbnailUrl = newTeam.thumbnailUrl
        this.members = newTeam.members
        resolve({
          success: true,
          team: this
        })
      } else {
        resolve({ error: newTeam.error || 'API return not Team obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.teams.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Team have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
