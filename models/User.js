/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class User {

  constructor(_id, username, password, fullname, phone, thumbnailUrl, projectsId, create_date) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.username = data.username
      this.password = data.password
      this.fullname = data.fullname
      this.phone = data.phone
      this.thumbnailUrl = data.thumbnailUrl
      this.projectsId = data.projectsId
      this.create_date = data.create_date

      return
    }

    this._id = _id
    this.username = username
    this.password = password
    this.fullname = fullname
    this.phone = phone
    this.thumbnailUrl = thumbnailUrl
    this.projectsId = projectsId
    this.create_date = create_date
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newUser = {}
      if (this._id) {
        newUser = await ctx.$api.users.update(this)
      } else {
        newUser = await ctx.$api.users.create(this)
      }
      if (newUser instanceof User) {
        this._id = newUser._id
        this.username = newUser.username
        this.password = newUser.password
        this.fullname = newUser.fullname
        this.phone = newUser.phone
        this.thumbnailUrl = newUser.thumbnailUrl
        this.projectsId = newUser.projectsId
        this.create_date = newUser.create_date
        resolve({
          success: true,
          user: this
        })
      } else {
        resolve({ error: newUser.error || 'API return not User obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.users.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'User have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
