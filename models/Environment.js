/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class Environment {

  constructor(_id, name, thumbnailUrl, type, rotation, lights) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.name = data.name
      this.thumbnailUrl = data.thumbnailUrl
      this.type = data.type
      this.rotation = data.rotation
      this.lights = data.lights

      return
    }

    this._id = _id
    this.name = name
    this.thumbnailUrl = thumbnailUrl
    this.type = type
    this.rotation = rotation
    this.lights = lights

  }

  save(ctx) {
    return new Promise(async resolve => {
      let newEnvironment = {}
      if (this._id) {
        newEnvironment = await ctx.$api.environments.update(this)
      } else {
        newEnvironment = await ctx.$api.environments.create(this)
      }
      if (newEnvironment instanceof Environment) {
        this._id = newEnvironment._id
        this.name = newEnvironment.name
        this.thumbnailUrl = newEnvironment.thumbnailUrl
        this.type = newEnvironment.type
        this.rotation = newEnvironment.rotation
        this.lights = newEnvironment.lights
        resolve({
          success: true,
          environment: this
        })
      } else {
        resolve({ error: newEnvironment.error || 'API return not Environment obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.environments.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Environment have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
