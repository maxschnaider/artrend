/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class Project {

  constructor(_id, name, authorId, thumbnailUrl, sceneId, accessType, invitedUsers, createDate) {
    if (arguments.length === 1) {
      let data = _id
      this._id = data._id
      this.name = data.name
      this.authorId = data.authorId
      this.thumbnailUrl = data.thumbnailUrl
      this.sceneId = data.sceneId
      this.accessType = data.accessType
      this.invitedUsers = data.invitedUsers
      this.createDate = data.createDate

      return
    }

    this._id = _id
    this.name = name
    this.authorId = authorId
    this.thumbnailUrl = thumbnailUrl
    this.sceneId = sceneId
    this.accessType = accessType
    this.invitedUsers = invitedUsers
    this.createDate = createDate
  }

  save(ctx) {
    return new Promise(async resolve => {
      let newProject = {}
      if (this._id) {
        newProject = await ctx.$api.projects.update(this)
      } else {
        newProject = await ctx.$api.projects.create(this)
      }
      if (newProject instanceof Project) {
        this._id = newProject._id
        this.name = newProject.name
        this.authorId = newProject.authorId
        this.thumbnailUrl = newProject.thumbnailUrl
        this.sceneId = newProject.sceneId
        this.accessType = newProject.accessType
        this.invitedUsers = newProject.invitedUsers
        this.createDate = newProject.createDate
        resolve({
          success: true,
          project: this
        })
      } else {
        resolve({ error: newProject.error || 'API return not Project obj' })
      }
    })
  }

  remove(ctx) {
    return new Promise(async resolve => {
      if (this._id) {
        const res = await ctx.$api.scenes.remove(this)
        resolve({
          error: res.error,
          success: res.error === undefined
        })
      } else {
        resolve({ error: 'Project have no id' })
      }
    })
  }

  jsonData() {
    return JSON.parse(JSON.stringify(this))
  }
}
