importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/0e4338761429b4eb16ac.css",
    "revision": "d41d8cd98f00b204e9800998ecf8427e"
  },
  {
    "url": "/_nuxt/1c9f470f5d60463e57bf.js",
    "revision": "9112e444fad53195d33955be5dd58bbe"
  },
  {
    "url": "/_nuxt/219b714fb98ecfe450a6.js",
    "revision": "d0e3be22835a0555d913b446d6ad6cc9"
  },
  {
    "url": "/_nuxt/2476e0067663c0a901d3.js",
    "revision": "8e1fbc08c17d61079e66118764189801"
  },
  {
    "url": "/_nuxt/515b8f5e63daa8d30b68.css",
    "revision": "0e178978e023fa3af833c3d382ad6cc5"
  },
  {
    "url": "/_nuxt/5b8f8c6455323224e005.css",
    "revision": "d41d8cd98f00b204e9800998ecf8427e"
  },
  {
    "url": "/_nuxt/63992d940894e0854252.css",
    "revision": "d30e1a3f33a6a2f5d6ddc0d42a5eca3f"
  },
  {
    "url": "/_nuxt/63a2d5bf8815bc55be61.js",
    "revision": "4868a0495e2877800412d1baddef9258"
  },
  {
    "url": "/_nuxt/6fc73a9279a87c558366.js",
    "revision": "025d3785c5031b817a6d52bfb3ef85dd"
  },
  {
    "url": "/_nuxt/78eb9a2468e312a6455a.js",
    "revision": "8cbae026dc48f754d4dcc03362eceafb"
  },
  {
    "url": "/_nuxt/8b5fefc0d2e9692ef7af.css",
    "revision": "d41d8cd98f00b204e9800998ecf8427e"
  },
  {
    "url": "/_nuxt/9fb9c0b00ae92ad91aa1.js",
    "revision": "670c518c3a5d6b9d12bc3334a678a19b"
  },
  {
    "url": "/_nuxt/a7e5f10def58b9426c93.js",
    "revision": "c75dfaacfe264a22a5121a2ee8f3c7d4"
  },
  {
    "url": "/_nuxt/bc6f44827209e70a637f.js",
    "revision": "62aef01d94a56181a3bfc1757b71cd6d"
  },
  {
    "url": "/_nuxt/d0388a59134f56104eac.js",
    "revision": "3460ad43eb50c3d0c8fe1a91781ab235"
  },
  {
    "url": "/_nuxt/e21e39aeed0c087407b8.js",
    "revision": "1e90deb145db136f2f10827bdd2e19e2"
  },
  {
    "url": "/_nuxt/f302605a7c5294ec2588.js",
    "revision": "69514b432faf95cd1101943c343b2f0c"
  },
  {
    "url": "/_nuxt/f381cb09b9beb9b90da8.js",
    "revision": "0a88d2025275e35cefeb27da778b7f8b"
  },
  {
    "url": "/_nuxt/f3d32db50e1c6b7a4cce.js",
    "revision": "b62566a6fb0305fef78564e08b418bdb"
  },
  {
    "url": "/_nuxt/fa4588b6921064029ef9.js",
    "revision": "1fa7d09c03709f62a5eddfde25e5b368"
  }
], {
  "cacheId": "artrend",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
