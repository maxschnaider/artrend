/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Material from '~/models/Material'

export default class MaterialsService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.materialsEndpoint
    this.vuexModulePath = Config.materialsEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const materialsData = res.data.materials
    if (materialsData) {
      const materials = []
      for (const materialData of materialsData) {
        materials.push(new Material(materialData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', materials)
      return materials
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params)
    const materialData = res.data.material
    if (materialData) {
      const material = new Material(materialData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', materialData)
      return material
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const materialData = res.data.material
    if (materialData) {
      const material = new Material(materialData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', material)
      return material
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const materialData = res.data.material
    if (materialData) {
      return new Material(materialData)
    }
    return res.data
  }
}

function createFormData(obj, form, namespace) {
  const fd = form || new FormData()
  let formKey

  for (const property in obj) {
    if (obj.hasOwnProperty(property) && obj[property]) {
      if (namespace) {
        formKey = namespace + '[' + property + ']'
      } else {
        formKey = property
      }

      if (obj[property] instanceof Date) {
        fd.append(formKey, obj[property].toISOString())
      }
      else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        this.createFormData(obj[property], fd, formKey)
      } else {
        fd.append(formKey, obj[property])
      }
    }
  }
  return fd
}
