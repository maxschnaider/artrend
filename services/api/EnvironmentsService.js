/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Environment from '~/models/Environment'

export default class EnvironmentsService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.environmentsEndpoint
    this.vuexModulePath = Config.environmentsEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const environmentsData = res.data.environments
    if (environmentsData) {
      const environments = []
      for (const environmentData of environmentsData) {
        environments.push(new Environment(environmentData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', environments)
      return environments
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    const environmentData = res.data.environment
    if (environmentData) {
      const environment = new Environment(environmentData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', environment)
      return environment
    }
    return res.data
  }

  async update (params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const environmentData = res.data.environment
    if (environmentData) {
      const environment = new Environment(environmentData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', environment)
      return environment
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const environmentData = res.data.environment
    if (environmentData) {
      return new Environment(environmentData)
    }
    return res.data
  }
}
