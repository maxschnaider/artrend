/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Team from '~/models/Team'

export default class TeamsService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.teamsEndpoint
    this.vuexModulePath = Config.teamsEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const teamsData = res.data.teams
    if (teamsData) {
      const teams = []
      for (const teamData of teamsData) {
        teams.push(new Team(teamData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', teams)
      return teams
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params)
    const teamData = res.data.team
    if (teamData) {
      const team = new Team(teamData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', team)
      return team
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const teamData = res.data.team
    if (teamData) {
      const team = new Team(teamData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', team)
      return team
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const teamData = res.data.team
    if (teamData) {
      return new Team(teamData)
    }
    return res.data
  }
}
