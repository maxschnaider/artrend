/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Surface from '~/models/Surface'

export default class SurfacesService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.surfacesEndpoint
    this.vuexModulePath = Config.surfacesEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const surfacesData = res.data.surfaces
    if (surfacesData) {
      const surfaces = []
      for (const surfacesData of surfacesData) {
        surfaces.push(new Surface(surfacesData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', surfaces)
      return surfaces
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    const surfaceData = res.data.surface
    if (surfaceData) {
      const surface = new Surface(surfaceData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', surface)
      return surface
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const surfaceData = res.data.surface
    if (surfaceData) {
      const surface = new Surface(surfaceData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', surface)
      return surface
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const surfaceData = res.data.surface
    if (surfaceData) {
      return new Surface(surfaceData)
    }
    return res.data
  }
}
