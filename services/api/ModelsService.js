/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Model from '~/models/Model'

export default class ModelsService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.modelsEndpoint
    this.vuexModulePath = Config.modelsEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const modelsData = res.data.models
    if (modelsData) {
      const models = []
      for (const modelData of modelsData) {
        models.push(new Model(modelData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', models)
      return models
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    const modelData = res.data.model
    if (modelData) {
      const model = new Model(modelData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', model)
      return model
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const modelData = res.data.model
    if (modelData) {
      const model = new Model(modelData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', model)
      return model
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const modelData = res.data.model
    if (modelData) {
      return new Model(modelData)
    }
    return res.data
  }
}
