/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import MaterialCategory from '~/models/MaterialCategory'

export default class MaterialCategoriesService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.materialCategoriesEndpoint
    this.vuexModulePath = Config.materialCategoriesEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const materialCategoriesData = res.data.materialCategories
    if (materialCategoriesData) {
      const materialCategories = []
      for (const materialCategoryData of materialCategoriesData) {
        materialCategories.push(new MaterialCategory(materialCategoryData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', materialCategories)
      return materialCategories
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params)
    const materialCategoryData = res.data.materialCategory
    if (materialCategoryData) {
      const materialCategory = new MaterialCategory(materialCategoryData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', materialCategory)
      return materialCategory
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const materialCategoryData = res.data.materialCategory
    if (materialCategoryData) {
      const materialCategory = new MaterialCategory(materialCategoryData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', materialCategory)
      return materialCategory
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const materialCategoryData = res.data.materialCategory
    if (materialCategoryData) {
      return new MaterialCategory(materialCategoryData)
    }
    return res.data
  }
}
