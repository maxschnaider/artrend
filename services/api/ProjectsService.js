/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Project from '~/models/Project'

export default class ProjectsService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.projectsEndpoint
    this.vuexModulePath = Config.projectsEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const projectsData = res.data.projects
    if (projectsData) {
      const projects = []
      for (const projectData of projectsData) {
        projects.push(new Project(projectData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'setCommonProjects', projects)
      return projects
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params,
      // { headers: { 'Content-Type': 'multipart/form-data' } }
      )
    const projectData = res.data.project
    if (projectData) {
      const project = new Project(projectData)
      this.ctx.app.store.commit(this.vuexModulePath + 'addUserProject', project)
      this.ctx.app.store.commit(this.vuexModulePath + 'addCommonProject', project)
      return project
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const projectData = res.data.project
    if (projectData) {
      const project = new Project(projectData)
      this.ctx.app.store.commit(this.vuexModulePath + 'updateUserProject', project)
      return project
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'removeUserProject', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const projectData = res.data.project
    if (projectData) {
      return new Project(projectData)
    }
    return res.data
  }

  async getAllForUser() {
    const projects = []
    for (const projectId of this.ctx.app.store.state.auth.user.projectsId) {
      const project = await this.getById(projectId)
      if (project instanceof Project) {
        projects.push(project)
      }
    }
    this.ctx.app.store.commit(this.vuexModulePath + 'setUserProjects', projects)
    return projects
  }
}
