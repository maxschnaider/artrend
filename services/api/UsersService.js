/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import User from '~/models/User'

export default class UsersService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.usersEndpoint
    this.vuexModulePath = Config.usersEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const usersData = res.data.users
    if (usersData) {
      const users = []
      for (const userData of usersData) {
        users.push(new User(userData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', users)
      return users
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params)
    const userData = res.data.user
    if (userData) {
      const user = new User(userData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', user)
      return user
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const userData = res.data.user
    if (userData) {
      const user = new User(userData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', user)
      return user
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const userData = res.data.user
    if (userData) {
      return new User(userData)
    }
    return res.data
  }
}
