/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Scene from '~/models/Scene'

export default class ScenesService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.scenesEndpoint
    this.vuexModulePath = Config.scenesEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const scenesData = res.data.scenes
    if (scenesData) {
      const scenes = []
      for (const sceneData of scenesData) {
        scenes.push(new Scene(sceneData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', scenes)
      return scenes
    }
    return res.data
  }

  async create(params) {
    const res = await this.ctx.$axios.post(this.endpointUrl, params)
    const sceneData = res.data.scene
    if (sceneData) {
      const scene = new Scene(sceneData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', scene)
      return scene
    }
    return res.data
  }

  async update(params) {
    const res = await this.ctx.$axios.put(this.endpointUrl + params._id, params)
    const sceneData = res.data.scene
    if (sceneData) {
      const scene = new Scene(sceneData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', scene)
      return scene
    }
    return res.data
  }

  async remove(params) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + params._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', params)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const sceneData = res.data.scene
    if (sceneData) {
      return new Scene(sceneData)
    }
    return res.data
  }
}
