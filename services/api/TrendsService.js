/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'
import Trend from '~/models/Trend'

export default class TrendsService {
  constructor(ctx) {
    this.ctx = ctx
    this.endpointUrl = Config.apiBaseUrl + Config.trendsEndpoint
    this.vuexModulePath = Config.trendsEndpoint
  }

  async getAll() {
    const res = await this.ctx.$axios.get(this.endpointUrl)
    const trendsData = res.data.trends
    if (trendsData) {
      let trends = []
      for (const trendData of trendsData) {
        trends.push(new Trend(trendData))
      }
      this.ctx.app.store.commit(this.vuexModulePath + 'set', trends)
      return trends
    }
    return res.data
  }

  async create(data) {
    const res = await this.ctx.$axios.post(this.endpointUrl, data)
    const trendData = res.data.trend
    if (trendData) {
      const trend = new Trend(trendData)
      this.ctx.app.store.commit(this.vuexModulePath + 'add', trend)
      return trend
    }
    return res.data
  }

  async update(data) {
    const res = await this.ctx.$axios.put(this.endpointUrl + data._id, data)
    const trendData = res.data.trend
    if (trendData) {
      const trend = new Trend(trendData)
      this.ctx.app.store.commit(this.vuexModulePath + 'update', trend)
      return trend
    }
    return res.data
  }

  async remove(data) {
    const res = await this.ctx.$axios.delete(this.endpointUrl + data._id)
    if (!res.data.error) {
      this.ctx.app.store.commit(this.vuexModulePath + 'remove', data)
    }
    return res.data
  }

  async getById(id) {
    const res = await this.ctx.$axios.get(this.endpointUrl + id)
    const trendData = res.data.trend
    if (trendData) {
      return new Trend(trendData)
    }
    return res.data
  }
}
