/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

import Config from './editor/Config'
import Loader from './editor/Loader'
import History from './editor/History'
import Commander from './editor/Commander'
import Viewport from './editor/Viewport'

export function newEditor(ctx) {
  let editor = new Object()

  editor.init = function (domElement) {
    const DEFAULT_CAMERA = new THREE.PerspectiveCamera(50, domElement.clientWidth / domElement.clientHeight, 0.01, 1000)
    DEFAULT_CAMERA.name = 'DefaultCamera'
    DEFAULT_CAMERA.position.set(0, 5, 25)
    DEFAULT_CAMERA.lookAt(new THREE.Vector3())
    editor.DEFAULT_CAMERA = DEFAULT_CAMERA

    editor.camera = editor.DEFAULT_CAMERA.clone()
    editor.domElement = domElement
    editor.ctx = ctx
    editor.config = new Config()
    editor.loader = new Loader(editor)
    editor.history = new History(editor)
    editor.commander = new Commander(editor)
    editor.viewport = new Viewport(editor)

    // scene
    editor.scene = new THREE.Scene()
    editor.scene.name = 'MainScene'
    editor.scene.background = new THREE.Color(editor.config.sceneBackgroundColor)

    // renderer
    editor.renderer = new THREE.WebGLRenderer({
      antialias: editor.config.rendererAntialias,
      preserveDrawingBuffer: true
    })
    editor.renderer.setSize(editor.domElement.clientWidth, editor.domElement.clientHeight)
    editor.renderer.shadowMap.enabled = true;
    editor.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    editor.domElement.appendChild(editor.renderer.domElement)

    // graphics
    editor.objects = {}
    editor.geometries = {}
    editor.materials = {}
    editor.scripts = {}
    editor.helpers = {}
    editor.sceneHelpers = new THREE.Scene()

    // architecture logic
    editor.modelData = {}
    editor.modelMesh = {}
    editor.projectData = {}
    editor.sceneData = {}
    editor.trendData = {}

    editor.setLight()
    editor.setEnvironment()
    eventListenersAdd()
  }

  editor.play = function () {
    editor.config.animate = true
    editor.viewport.animate()
  }

  editor.stop = function () {
    editor.config.animate = false
  }

  editor.deinit = function () {
    editor.clear()
    eventListenersRemove()
  }

  editor.setLight = function () {
    let ambientLight = new THREE.AmbientLight(0xffffff, 0.55)
    ambientLight.name = 'ambientLight'
    editor.scene.add(ambientLight)

    let hemisphereLight = new THREE.HemisphereLight(0xbedff6, 0xffc04c , 0.1)
    hemisphereLight.name = 'hemisphereLight'
    hemisphereLight.position.set(0,-1,0)
    editor.scene.add(hemisphereLight)

    let sunLight = new THREE.DirectionalLight(0xffffd4, 0.7)
    sunLight.name = 'sunLight'
    sunLight.castShadow = true
    sunLight.position.set(10,10,10)
    sunLight.shadow.camera = new THREE.OrthographicCamera(-20, 20, 20, -20, 0.5, 1000)
    sunLight.shadow.mapSize.x = 3000
    sunLight.shadow.mapSize.y = 3000
    // sunLight.shadow.bias = 0.0000001
    // sunLight.shadow.radius = 1.2
    editor.scene.add(sunLight)

    // let sightLight = new THREE.PointLight(0xffffff, 0.05)
    // sightLight.name = 'sightLight'
    // sightLight.castShadow = true
    // sightLight.shadow.bias = 0.0000001
    // sightLight.shadow.radius = 3.5
    // editor.scene.add(sightLight)

    editor.viewport.sceneGraphChanged()
  }

  editor.setEnvironment = function (envName) {
    // if (editor.loader.startLoading() === false) {
    //   return
    // }
    const path = "https://threejs.org/examples/textures/cube/Park2/"
    let format = '.jpg'
    const urls = [
      path + 'posx' + format, path + 'negx' + format,
      path + 'posy' + format, path + 'negy' + format,
      path + 'posz' + format, path + 'negz' + format
    ]

    // const path = "../editor/assets/textures/environment/" + envName + '/'
    // const urls = [path + "posx.jpg", path + "negx.jpg",
    //   path + "posy.jpg", path + "negy.jpg",
    //   path + "posz.jpg", path + "negz.jpg"]

    if (urls[0] in this.loader.loadedURLs) {
      let cubeTexture = this.loader.loadedURLs[urls[0]]
      editor.loader.updateLoading()
      editor.scene.background = cubeTexture
      editor.envMap = cubeTexture
      editor.viewport.sceneGraphChanged()
      return
    }

    editor.loader.loadCubeTexture(urls,
      function (cubeTexture, bool) {
        if (!bool) {
          return
        }
        editor.scene.background = cubeTexture
        editor.envMap = cubeTexture
        editor.envMap.format = THREE.RGBFormat
        editor.viewport.sceneGraphChanged()
      })
  }

  editor.setScene = function (scene) {
    editor.scene.uuid = scene.uuid
    editor.scene.name = scene.name
    if (scene.background !== null) editor.scene.background = scene.background.clone()
    if (scene.fog !== null) editor.scene.fog = scene.fog.clone()
    editor.scene.userData = JSON.parse(JSON.stringify(scene.userData))
    while (scene.children.length > 0) {
      editor.addObject(scene.children[0])
    }
    editor.viewport.sceneGraphChanged()
  }

  editor.selectMaterial = function (object, materialSlot) {
    let selectedMaterialSlot = editor.selectedMaterialSlot
    if (selectedMaterialSlot !== -1) {
      editor.selected.traverse(function(child) {
        if (child.material) {
          child.material[selectedMaterialSlot] = editor.tempMaterial
        }
      })
    }
    editor.select(object)
    editor.selectedMaterialSlot = materialSlot
    editor.selected.traverse(function(child) {
      if (child.material) {
        //child.material[materialIndex].wireframe = true
        editor.tempMaterial = child.material[materialSlot]
        child.material[materialSlot] = new THREE.MeshStandardMaterial({color: 0xff0000})
        editor.selectedMaterialSlot = materialSlot
      }
    })
    // editor.signals.showSurfacePanel.dispatch(true)
  }

  editor.deselectMaterial = function () {
    if (editor.selectedMaterialSlot !== -1) {
      editor.selected.traverse(function(child) {
        if (child.material) {
          child.material[editor.selectedMaterialSlot] = editor.tempMaterial
        }
      })
    }
    // editor.signals.showSurfacePanel.dispatch(false)
    editor.selectedMaterialSlot = -1
  }

  editor.addObject = function (object) {
    object.traverse(function (child) {
      if (child.geometry !== undefined) editor.addGeometry(child.geometry)
      if (child.material !== undefined) editor.addMaterial(child.material)
      editor.addHelper(child)
      editor.objects[child.uuid] = child
    })
    editor.scene.add(object)
    editor.viewport.objectAdded(object)
    editor.viewport.sceneGraphChanged()
  }

  editor.moveObject = function (object, parent, before) {
    if (parent === undefined) {
      parent = editor.scene
    }
    parent.add(object)
    if (before !== undefined) {
      let index = parent.children.indexOf(before)
      parent.children.splice(index, 0, object)
      parent.children.pop()
    }
    editor.viewport.sceneGraphChanged()
  }

  editor.nameObject = function (object, name) {
    object.name = name
    editor.viewport.sceneGraphChanged()
  }

  editor.removeObject = function (object) {
    if (object.parent === null) return
    object.traverse(function (child) {
      editor.removeHelper(child)
    })
    object.parent.remove(object)
    editor.viewport.objectRemoved(object)
    editor.viewport.sceneGraphChanged()
  }

  editor.addGeometry = function (geometry) {
    editor.geometries[geometry.uuid] = geometry
  }

  editor.setGeometryName = function (geometry, name) {
    geometry.name = name
    editor.viewport.sceneGraphChanged()
  }

  editor.addMaterial = function (material) {
    editor.materials[material.uuid] = material
  }

  editor.setMaterialName = function (material, name) {
    material.name = name
    editor.viewport.sceneGraphChanged()
  }

  editor.addTexture = function (texture) {
    editor.textures[texture.uuid] = texture
  }

  editor.addHelper = function () {
    let geometry = new THREE.SphereBufferGeometry(2, 4, 2)
    let material = new THREE.MeshBasicMaterial({color: 0xff0000, visible: false})
    return function (object) {
      let helper
      if (object instanceof THREE.Camera) {
        helper = new THREE.CameraHelper(object, 1)
      } else if (object instanceof THREE.PointLight) {
        helper = new THREE.PointLightHelper(object, 1)
      } else if (object instanceof THREE.DirectionalLight) {
        helper = new THREE.DirectionalLightHelper(object, 1)
      } else if (object instanceof THREE.SpotLight) {
        helper = new THREE.SpotLightHelper(object, 1)
      } else if (object instanceof THREE.HemisphereLight) {
        helper = new THREE.HemisphereLightHelper(object, 1)
      } else if (object instanceof THREE.SkinnedMesh) {
        helper = new THREE.SkeletonHelper(object)
      } else {
        return
      }
      let picker = new THREE.Mesh(geometry, material)
      picker.name = 'picker'
      picker.userData.object = object
      helper.add(picker)
      editor.sceneHelpers.add(helper)
      editor.helpers[object.id] = helper
      editor.viewport.helperAdded(helper)
    }
  }()

  editor.removeHelper = function (object) {
    if (editor.helpers[object.id] !== undefined) {
      let helper = this.helpers[object.id]
      helper.parent.remove(helper)
      delete editor.helpers[object.id]
      editor.viewport.helperRemoved(helper)
    }
  }

  editor.addScript = function (object, script) {
    if (editor.scripts[object.uuid] === undefined) {
      editor.scripts[object.uuid] = []
    }
    editor.scripts[object.uuid].push(script)
    // editor.signals.scriptAdded.dispatch(script)
  }

  editor.removeScript = function (object, script) {
    if (editor.scripts[object.uuid] === undefined) return
    let index = this.scripts[object.uuid].indexOf(script)
    if (index !== - 1) {
      editor.scripts[object.uuid].splice(index, 1)
    }
    // editor.signals.scriptRemoved.dispatch(script)
  }

  editor.getObjectMaterial = function (object, slot) {
    let material = object.material
    if (Array.isArray(material)) {
      material = material[slot]
    }
    return material
  }

  editor.setObjectMaterial = function (object, slot, newMaterial) {
    if (Array.isArray(object.material)) {
      // if (object.material[slot].uuid === newMaterial.uuid) {
      //   console.log('Material is already applied')
      // return
      // }
      object.material[slot] = newMaterial
    } else {
      object.material = newMaterial
    }
  }

  editor.select = function (object) {
    if (editor.selected === object) return
    var uuid = null
    if (object !== null) {
      uuid = object.uuid
    } else {
      editor.deselectMaterial()
    }
    editor.selected = object
    editor.config.selected = uuid
    editor.viewport.objectSelected(object)
  }

  editor.selectById = function (id) {
    if (id === editor.camera.id) {
      editor.select(editor.camera)
      return
    }
    editor.select(editor.scene.getObjectById(id, true))
  }

  editor.selectByUuid = function (uuid) {
    editor.scene.traverse(function (child) {
      if (child.uuid === uuid) {
        editor.select(child)
      }
    })
  }

  editor.deselect = function () {
    editor.select(null)
  }

  editor.focus = function (object) {
    editor.viewport.objectFocused(object)
  }

  editor.focusById = function (id) {
    editor.focus(editor.scene.getObjectById(id, true))
  }

  editor.clear = function () {
    editor.domElement = null
    editor.config = new Config()
    editor.history.clear()
    // editor.storage.clear()
    editor.camera.copy(editor.DEFAULT_CAMERA)
    editor.scene.fog = null
    let objects = editor.scene.children
    while (objects.length > 0) {
      this.removeObject(objects[0])
    }
    editor.object = {}
    editor.objects = {}
    editor.geometries = {}
    editor.materials = {}
    editor.textures = {}
    editor.scripts = {}

    editor.modelData = {}
    editor.modelMesh = {}
    editor.projectData = {}
    editor.sceneData = {}
    editor.trendData = {}

    editor.selectedMaterialSlot = -1
    editor.deselect()
    editor.viewport.editorCleared()
    editor.setLight()
  }

  editor.fromJSON = function (json) {
    let loader = new THREE.ObjectLoader()
    if (json.scene === undefined) {
      editor.setScene(loader.parse(json))
      return
    }
    let camera = loader.parse(json.camera)
    editor.camera.copy(camera)
    editor.camera.aspect = editor.DEFAULT_CAMERA.aspect
    editor.camera.updateProjectionMatrix()
    editor.history.fromJSON(json.history)
    editor.scripts = json.scripts
    editor.setScene(loader.parse(json.scene))
  }

  editor.toJSON = function () {
    let scene = this.scene
    let scripts = this.scripts
    for (let key in scripts) {
      let script = scripts[key]
      if (script.length === 0 || scene.getObjectByProperty('uuid', key) === undefined) {
        delete scripts[key]
      }
    }
    return {
      metadata: {},
      project: {
        gammaInput: editor.config.rendererGammaInput,
        gammaOutput: editor.config.rendererGammaOutput,
        shadows: editor.config.rendererShadows,
        vr: editor.config.VR
      },
      camera: editor.camera.toJSON(),
      scene: editor.scene.toJSON(),
      scripts: editor.scripts,
      history: editor.history.toJSON()
    }
  }

  editor.objectByUuid = function (uuid) {
    return editor.scene.getObjectByProperty('uuid', uuid, true)
  }

  editor.execute = function (cmd, optionalName) {
    return editor.history.execute(cmd, optionalName)
  }

  editor.undo = function () {
    editor.history.undo()
  }

  editor.redo = function () {
    editor.history.redo()
  }


// eventListeners
  function onWindowResize() {
    editor.viewport.windowResize()
  }

  function onDocumentDragOver(event) {
    event.preventDefault()
    event.dataTransfer.dropEffect = 'copy'
  }

  function onDocumentDrop(event) {
    event.preventDefault()
    editor.loader.loadFiles(event.dataTransfer.files)
  }

  function eventListenersAdd() {
    window.addEventListener('resize', onWindowResize, false)
    document.addEventListener('dragover', onDocumentDragOver, false)
    document.addEventListener('drop', onDocumentDrop, false)
  }

  function eventListenersRemove() {
    window.removeEventListener('resize', onWindowResize, false)
    document.removeEventListener('dragover', onDocumentDragOver, false)
    document.removeEventListener('drop', onDocumentDrop, false)
  }

  return editor
}
