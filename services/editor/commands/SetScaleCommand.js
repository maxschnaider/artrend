/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetScaleCommand {

  constructor (editor) {

    this.type = 'SetScaleCommand';
    this.name = 'Set Scale';
    this.updatable = true;

    this.init = function (object, newScale, optionalOldScale) {

      this.object = object;

      if (object !== undefined && newScale !== undefined) {
        this.oldScale = object.scale.clone();
        this.newScale = newScale.clone();
      }

      if (optionalOldScale !== undefined) {
        this.oldScale = optionalOldScale.clone();
      }

      return this.clone();

    };

    this.execute = function () {

      this.object.scale.copy(this.newScale);
      this.object.updateMatrixWorld(true);
      // editor.signals.objectChanged.dispatch(this.object);

    };

    this.undo = function () {

      this.object.scale.copy(this.oldScale);
      this.object.updateMatrixWorld(true);
      // editor.signals.objectChanged.dispatch(this.object);

    };

    this.update = function (command) {

      this.newScale.copy(command.newScale);

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.oldScale = this.oldScale.toArray();
      output.newScale = this.newScale.toArray();

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.oldScale = new THREE.Vector3().fromArray(json.oldScale);
      this.newScale = new THREE.Vector3().fromArray(json.newScale);

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
