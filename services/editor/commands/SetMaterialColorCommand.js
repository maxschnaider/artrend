/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class SetMaterialColorCommand {

  constructor (editor) {

    this.type = 'SetMaterialColorCommand';
    this.name = 'Set Material Color';
    this.updatable = true;

    this.init = function (object, attributeName, newValue, materialSlot) {

      this.name = 'Set Material.' + attributeName;
      this.object = object;
      this.material = editor.getObjectMaterial(object, materialSlot);
      this.oldValue = (this.material !== undefined) ? this.material[attributeName].getHex() : undefined;
      this.newValue = newValue;
      this.attributeName = attributeName;

      return this.clone();

    };

    this.execute = function () {

      this.material[this.attributeName].setHex(this.newValue);
      // editor.signals.materialChanged.dispatch(this.material);

    };

    this.undo = function () {

      this.material[this.attributeName].setHex(this.oldValue);
      // editor.signals.materialChanged.dispatch(this.material);

    };

    this.update = function (cmd) {

      this.newValue = cmd.newValue;

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.attributeName = this.attributeName;
      output.oldValue = this.oldValue;
      output.newValue = this.newValue;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.attributeName = json.attributeName;
      this.oldValue = json.oldValue;
      this.newValue = json.newValue;

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };
  }

};
