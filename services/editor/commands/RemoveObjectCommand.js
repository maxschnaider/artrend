/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class RemoveObjectCommand {

  constructor (editor) {

    this.type = 'RemoveObjectCommand';
    this.name = 'Remove Object';

    this.init = function (object) {

      this.object = object;
      this.parent = (object !== undefined) ? object.parent : undefined;

      if (this.parent !== undefined) {
        this.index = this.parent.children.indexOf(this.object);
      }

      return this.clone();

    };

    this.execute = function () {

      this.object.traverse(function (child) {
        editor.removeHelper(child);
      });

      this.parent.remove(this.object);
      editor.select(this.parent);

      //  editor.signals.objectRemoved.dispatch(this.object);
      //  editor.signals.sceneGraphChanged.dispatch();

    };

    this.undo = function () {

      this.object.traverse(function (child) {

        if (child.geometry !== undefined) editor.addGeometry(child.geometry);
        if (child.material !== undefined) editor.addMaterial(child.material);

        editor.addHelper(child);

      });

      this.parent.children.splice(this.index, 0, this.object);
      this.object.parent = this.parent;
      editor.select(this.object);

      //  editor.signals.objectAdded.dispatch(this.object);
      //  editor.signals.sceneGraphChanged.dispatch();

    };

    this.toJSON = function () {

      var output =  editor.commander.toJSON(this);
      output.object = this.object.toJSON();
      output.index = this.index;
      output.parentUuid = this.parent.uuid;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);
      this.parent =  editor.objectByUuid(json.parentUuid);

      if (this.parent === undefined) {
        this.parent =  editor.scene;
      }

      this.index = json.index;
      this.object =  editor.objectByUuid(json.object.object.uuid);

      if (this.object === undefined) {
        let loader = new THREE.ObjectLoader();
        this.object = loader.parse(json.object);
      }

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
