/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class RemoveScriptCommand {

  constructor (editor) {

    this.type = 'RemoveScriptCommand';
    this.name = 'Remove Script';


    this.init = function (object, script) {

      this.object = object;
      this.script = script;

      if (this.object && this.script) {
        this.index = editor.scripts[this.object.uuid].indexOf(this.script);
      }

      return this.clone();

    };

    this.execute = function () {

      if (editor.scripts[this.object.uuid] === undefined) return;

      if (this.index !== - 1) {
        editor.scripts[this.object.uuid].splice(this.index, 1);
      }

      // editor.signals.scriptRemoved.dispatch(this.script);

    };

    this.undo = function () {

      if (editor.scripts[this.object.uuid] === undefined) {
        editor.scripts[this.object.uuid] = [];
      }

      editor.scripts[this.object.uuid].splice(this.index, 0, this.script);
      // editor.signals.scriptAdded.dispatch(this.script);

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.script = this.script;
      output.index = this.index;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.script = json.script;
      this.index = json.index;
      this.object = editor.objectByUuid(json.objectUuid);

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
       ),
        this
     );

      return newObject;

    };

  }
}
