/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class SetUuidCommand {

  constructor (editor) {

    this.type = 'SetUuidCommand';
    this.name = 'Update UUID';

    this.init = function (object, newUuid) {

      this.object = object;
      this.oldUuid = (object !== undefined) ? object.uuid : undefined;
      this.newUuid = newUuid;

      return this.clone();

    };

    this.execute = function () {

      this.object.uuid = this.newUuid;
      // editor.signals.objectChanged.dispatch(this.object);
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.undo = function () {

      this.object.uuid = this.oldUuid;
      // editor.signals.objectChanged.dispatch(this.object);
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.oldUuid = this.oldUuid;
      output.newUuid = this.newUuid;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.oldUuid = json.oldUuid;
      this.newUuid = json.newUuid;
      this.object = editor.objectByUuid(json.oldUuid);

      if (this.object === undefined) {
        this.object = editor.objectByUuid(json.newUuid);
      }

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
