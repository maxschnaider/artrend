/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetPositionCommand {

  constructor (editor) {

    this.type = 'SetPositionCommand';
    this.name = 'Set Position';
    this.updatable = true;
    this.object = null;

    this.init = function (object, newPosition, optionalOldPosition) {

      if (object !== undefined && newPosition !== undefined) {
        this.object = object;
        this.oldPosition = object.position.clone();
        this.newPosition = newPosition.clone();
      }

      if (optionalOldPosition !== undefined) {
        this.oldPosition = optionalOldPosition.clone();
      }

      return this.clone();

    };

    this.execute = function () {

      this.object.position.copy(this.newPosition);
      this.object.updateMatrixWorld(true);

      editor.viewport.sceneGraphChanged();

    };

    this.undo = function () {

      this.object.position.copy(this.oldPosition);
      this.object.updateMatrixWorld(true);

      editor.viewport.sceneGraphChanged();

    };

    this.update = function (command) {

      this.newPosition.copy(command.newPosition);

    };

    this.toJSON = function () {

      let output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.oldPosition = this.oldPosition.toArray();
      output.newPosition = this.newPosition.toArray();

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.oldPosition = new THREE.Vector3().fromArray(json.oldPosition);
      this.newPosition = new THREE.Vector3().fromArray(json.newPosition);

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
       ),
        this
     );

      return newObject;

    };

  }
}
