/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class SetGeometryValueCommand {

  constructor (editor) {

    this.type = 'SetGeometryValueCommand';
    this.name = 'Set Geometry Value';

    this.init = function (object, attributeName, newValue) {

      this.name = 'Set Geometry.' + attributeName;
      this.object = object;
      this.attributeName = attributeName;
      this.oldValue = (object !== undefined) ? object.geometry[attributeName] : undefined;
      this.newValue = newValue;

      return this.clone();

    };

    this.execute = function () {

      this.object.geometry[this.attributeName] = this.newValue;
      // editor.signals.objectChanged.dispatch(this.object);
      // editor.signals.geometryChanged.dispatch();
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.undo = function () {

      this.object.geometry[this.attributeName] = this.oldValue;
      // editor.signals.objectChanged.dispatch(this.object);
      // editor.signals.geometryChanged.dispatch();
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.attributeName = this.attributeName;
      output.oldValue = this.oldValue;
      output.newValue = this.newValue;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.attributeName = json.attributeName;
      this.oldValue = json.oldValue;
      this.newValue = json.newValue;

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
