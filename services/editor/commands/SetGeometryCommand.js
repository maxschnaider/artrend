/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetGeometryCommand {

  constructor (editor) {

    this.type = 'SetGeometryCommand';
    this.name = 'Set Geometry';
    this.updatable = true;

    this.init = function (object, newGeometry) {

      this.object = object;
      this.oldGeometry = (object !== undefined) ? object.geometry : undefined;
      this.newGeometry = newGeometry;

      return this.clone();

    };

    this.execute = function () {

      this.object.geometry.dispose();
      this.object.geometry = this.newGeometry;
      this.object.geometry.computeBoundingSphere();

      // editor.signals.geometryChanged.dispatch(this.object);
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.undo = function () {

      this.object.geometry.dispose();
      this.object.geometry = this.oldGeometry;
      this.object.geometry.computeBoundingSphere();

      // editor.signals.geometryChanged.dispatch(this.object);
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.update = function (cmd) {

      this.newGeometry = cmd.newGeometry;

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.oldGeometry = this.object.geometry.toJSON();
      output.newGeometry = this.newGeometry.toJSON();

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);

      this.oldGeometry = parseGeometry(json.oldGeometry);
      this.newGeometry = parseGeometry(json.newGeometry);

      function parseGeometry (data) {

        var loader = new THREE.ObjectLoader();
        return loader.parseGeometries([data])[data.uuid];

      }

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
