/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetMaterialCommand {
  constructor (editor) {
    this.type = 'SetMaterialCommand';
    this.name = 'New Material';

    this.init = function (object, materialSlot, material) {
      this.object = object;
      this.materialSlot = materialSlot;
      this.oldMaterial = editor.getObjectMaterial(object, materialSlot);
      this.material = material;

      return this.clone();
    };

    this.execute = function () {
      editor.setObjectMaterial(this.object, this.materialSlot, this.material);
      editor.viewport.materialChanged(this.material);
    };

    this.undo = function () {
      editor.setObjectMaterial(this.object, this.materialSlot, this.oldMaterial);
      editor.viewport.materialChanged(this.oldMaterial);
    };

    this.toJSON = function () {
      let output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.oldMaterial = this.oldMaterial.toJSON();
      output.material = this.material.toJSON();

      return output;
    };

    this.fromJSON = function (json) {
      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.oldMaterial = parseMaterial(json.oldMaterial);
      this.material = parseMaterial(json.material);

      function parseMaterial (json) {
        let loader = new THREE.ObjectLoader();
        let images = loader.parseImages(json.images);
        let textures  = loader.parseTextures(json.textures, images);
        let materials = loader.parseMaterials([json], textures);
        return materials[json.uuid];
      }
    };

    this.clone = function () {
      let newObject = Object.assign(Object.create(Object.getPrototypeOf(this)), this);
      return newObject;
    };
  }
}
