/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class AddObjectCommand {
  constructor (editor) {
    this.type = 'AddObjectCommand';
    this.name = 'Add Object';

    this.init = function (object) {
      this.object = object;
      this.name = 'Add Object: ' + object.name;

      return this.clone();
    };

    this.execute = function () {
      editor.addObject(this.object);
      editor.select(this.object);
    };

    this.undo = function () {
      editor.removeObject(this.object);
      editor.deselect();
    };

    this.toJSON = function () {
      let output =  editor.commander.toJSON(this);
      output.object = this.object.toJSON();

      return output;
    };

    this.fromJSON = function (json) {
      editor.commander.fromJSON(this, json);
      this.object =  editor.objectByUuid(json.object.object.uuid);

      if (this.object === undefined) {
        let loader = new THREE.ObjectLoader();
        this.object = loader.parse(json.object);
      }
    };

    this.clone = function () {
      let newObject = Object.assign(Object.create(Object.getPrototypeOf(this)), this);

      return newObject;
    };

  }
}
