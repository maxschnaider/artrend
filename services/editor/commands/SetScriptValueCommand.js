/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class SetScriptValueCommand {

  constructor (editor) {

    this.type = 'SetScriptValueCommand';
    this.name = 'Set Script Value';
    this.updatable = true;

    this.init = function (object, script, attributeName, newValue) {

      this.name = 'Set Script.' + attributeName;
      this.object = object;
      this.script = script;

      this.attributeName = attributeName;
      this.oldValue = (script !== undefined) ? script[this.attributeName] : undefined;
      this.newValue = newValue;

      return this.clone();

    };

    this.execute = function () {

      this.script[this.attributeName] = this.newValue;

      // editor.signals.scriptChanged.dispatch();

    };

    this.undo = function () {

      this.script[this.attributeName] = this.oldValue;

      // editor.signals.scriptChanged.dispatch();

    };

    this.update = function (cmd) {

      this.newValue = cmd.newValue;

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.index = editor.scripts[this.object.uuid].indexOf(this.script);
      output.attributeName = this.attributeName;
      output.oldValue = this.oldValue;
      output.newValue = this.newValue;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.oldValue = json.oldValue;
      this.newValue = json.newValue;
      this.attributeName = json.attributeName;
      this.object = editor.objectByUuid(json.objectUuid);
      this.script = editor.scripts[json.objectUuid][json.index];

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
       ),
        this
     );

      return newObject;

    };

  }
}
