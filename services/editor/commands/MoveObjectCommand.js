/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class MoveObjectCommand {

  constructor (editor) {

    this.type = 'MoveObjectCommand';
    this.name = 'Move Object';

    this.init = function (object, newParent, newBefore) {

      this.object = object;
      this.oldParent = object.parent;
      this.oldIndex = (this.oldParent !== undefined) ? this.oldParent.children.indexOf(this.object) : undefined;
      this.newParent = newParent;

      if (newBefore !== undefined) {
        this.newIndex = (newParent !== undefined) ? newParent.children.indexOf(newBefore) : undefined;
      } else {
        this.newIndex = (newParent !== undefined) ? newParent.children.length : undefined;
      }
      if (this.oldParent === this.newParent && this.newIndex > this.oldIndex) {
        this.newIndex--;
      }
      this.newBefore = newBefore;

      return this.clone();

    };

    this.execute = function () {

      this.oldParent.remove(this.object);

      var children = this.newParent.children;
      children.splice(this.newIndex, 0, this.object);
      this.object.parent = this.newParent;

      //  editor.signals.sceneGraphChanged.dispatch();

    };

    this.undo = function () {

      this.newParent.remove(this.object);

      var children = this.oldParent.children;
      children.splice(this.oldIndex, 0, this.object);
      this.object.parent = this.oldParent;

      //  editor.signals.sceneGraphChanged.dispatch();

    };

    this.toJSON = function () {

      var output = Command.prototype.toJSON.call(this);

      output.objectUuid = this.object.uuid;
      output.newParentUuid = this.newParent.uuid;
      output.oldParentUuid = this.oldParent.uuid;
      output.newIndex = this.newIndex;
      output.oldIndex = this.oldIndex;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object =  editor.objectByUuid(json.objectUuid);
      this.oldParent =  editor.objectByUuid(json.oldParentUuid);

      if (this.oldParent === undefined) {
        this.oldParent =  editor.scene;
      }

      this.newParent =  editor.objectByUuid(json.newParentUuid);

      if (this.newParent === undefined) {
        this.newParent =  editor.scene;
      }

      this.newIndex = json.newIndex;
      this.oldIndex = json.oldIndex;

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
