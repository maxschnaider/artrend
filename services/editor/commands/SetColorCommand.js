/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class SetColorCommand {

  constructor (editor) {

    this.type = 'SetColorCommand';
    this.name = 'Set Color';
    this.updatable = true;

    this.init = function (object, attributeName, newValue) {

      this.name = 'Set ' + attributeName;
      this.object = object;
      this.attributeName = attributeName;
      this.oldValue = (object !== undefined) ? this.object[this.attributeName].getHex() : undefined;
      this.newValue = newValue;

      return this.clone();

    };

    this.execute = function () {

      this.object[this.attributeName].setHex(this.newValue);
      // this.editor.signals.objectChanged.dispatch(this.object);

    };

    this.undo = function () {

      this.object[this.attributeName].setHex(this.oldValue);
      // this.editor.signals.objectChanged.dispatch(this.object);

    };

    this.update = function (cmd) {

      this.newValue = cmd.newValue;

    };

    this.toJSON = function () {

      var output = this.editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.attributeName = this.attributeName;
      output.oldValue = this.oldValue;
      output.newValue = this.newValue;

      return output;

    };

    this.fromJSON = function (json) {

      this.editor.commander.fromJSON(this, json);

      this.object = this.editor.objectByUuid(json.objectUuid);
      this.attributeName = json.attributeName;
      this.oldValue = json.oldValue;
      this.newValue = json.newValue;

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
