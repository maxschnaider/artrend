/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class SetSceneCommand {

  constructor (editor) {

    this.type = 'SetSceneCommand';
    this.name = 'Set Scene';
    this.cmdArray = [];

    this.init = function (scene) {

      if (scene !== undefined) {


        this.cmdArray.push(editor.commander.setUuidCommand.init(editor.scene, scene.uuid));
        this.cmdArray.push(editor.commander.setValueCommand.init(editor.scene, 'name', scene.name));
        this.cmdArray.push(editor.commander.setValueCommand.init(editor.scene, 'userData', JSON.parse(JSON.stringify(scene.userData))));

        while (scene.children.length > 0) {
          let child = scene.children.pop();
          this.cmdArray.push(editor.commander.addObjectCommand.init(child));
        }
      }

      return this.clone();

    };

    this.execute = function () {

      // editor.signals.sceneGraphChanged.active = false;

      for (var i = 0; i < this.cmdArray.length; i ++) {
        this.cmdArray[i].execute();
      }

      // editor.signals.sceneGraphChanged.active = true;
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.undo = function () {

      // editor.signals.sceneGraphChanged.active = false;

      for (var i = this.cmdArray.length - 1; i >= 0; i --) {
        this.cmdArray[i].undo();
      }

      // editor.signals.sceneGraphChanged.active = true;
      // editor.signals.sceneGraphChanged.dispatch();

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);
      var cmds = [];

      for (var i = 0; i < this.cmdArray.length; i ++) {
        cmds.push(this.cmdArray[i].toJSON());
      }

      output.cmds = cmds;

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);
      var cmds = json.cmds;

      for (var i = 0; i < cmds.length; i ++) {
        var cmd = new window[cmds[i].type]();	// creates a new object of type "json.type"
        cmd.fromJSON(cmds[i]);
        this.cmdArray.push(cmd);
      }

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}

