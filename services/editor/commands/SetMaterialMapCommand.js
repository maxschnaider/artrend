/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetMaterialMapCommand {

  constructor (editor) {

    this.type = 'SetMaterialMapCommand';
    this.name = 'Set Material Map';

    this.init = function (object, mapName, newMap, materialSlot, scale = null) {

      this.name = 'Set Material.' + mapName;
      this.object = object;
      this.material = editor.getObjectMaterial(object, materialSlot);
      this.oldMap = (object !== undefined) ? this.material[mapName] : undefined;
      this.newMap = newMap;
      this.mapName = mapName;
      this.scale = scale == null ? [0.1, 0.1] : scale;

      return this.clone();

    };

    this.execute = function () {

      this.newMap.wrapS = THREE.RepeatWrapping;
      this.newMap.wrapT = THREE.RepeatWrapping;
      this.newMap.repeat.set(this.scale[0], this.scale[1]);
      this.newMap.needUpdate = true;

      this.material[this.mapName] = this.newMap;
      this.material.needsUpdate = true;

      // editor.signals.materialChanged.dispatch(this.material);

    };

    this.undo = function () {

      this.material[this.mapName] = this.oldMap;
      this.material.needsUpdate = true;

      // editor.signals.materialChanged.dispatch(this.material);

    };

    this.toJSON = function () {

      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.mapName = this.mapName;
      output.newMap = serializeMap(this.newMap);
      output.oldMap = serializeMap(this.oldMap);

      return output;

      function serializeMap (map) {

        if (map === null || map === undefined) return null;

        let meta = {
          geometries: {},
          materials: {},
          textures: {},
          images: {}
        };

        let json = map.toJSON(meta);
        let images = extractFromCache(meta.images);
        if (images.length > 0) json.images = images;
        json.sourceFile = map.sourceFile;

        return json;

      }

      function extractFromCache (cache) {

        var values = [];

        for (var key in cache) {
          var data = cache[key];
          delete data.metadata;
          values.push(data);
        }

        return values;

      }
    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.mapName = json.mapName;
      this.oldMap = parseTexture(json.oldMap);
      this.newMap = parseTexture(json.newMap);

      function parseTexture (json) {

        var map = null;

        if (json !== null) {
          var loader = new THREE.ObjectLoader();
          var images = loader.parseImages(json.images);
          var textures  = loader.parseTextures([json], images);
          map = textures[json.uuid];
          map.sourceFile = json.sourceFile;
        }

        return map;
      }

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
