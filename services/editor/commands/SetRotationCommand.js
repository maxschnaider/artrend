/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetRotationCommand {

  constructor (editor) {

    this.type = 'SetRotationCommand';
    this.name = 'Set Rotation';
    this.updatable = true;

    this.init = function (object, newRotation, optionalOldRotation) {

      this.object = object;
      this.oldRotation = object.rotation.clone();
      this.newRotation = newRotation.clone();

      if (optionalOldRotation !== undefined) {
        this.oldRotation = optionalOldRotation.clone();
      }

      return this.clone();

    };

    this.execute = function () {

      this.object.rotation.copy(this.newRotation);
      this.object.updateMatrixWorld(true);

      editor.viewport.sceneGraphChanged();

    };

    this.undo = function () {

      this.object.rotation.copy(this.oldRotation);
      this.object.updateMatrixWorld(true);

      editor.viewport.sceneGraphChanged();

    };

    this.update = function (command) {

      this.newRotation.copy(command.newRotation);

    };

    this.toJSON = function () {

      let output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.oldRotation = this.oldRotation.toArray();
      output.newRotation = this.newRotation.toArray();

      return output;

    };

    this.fromJSON = function (json) {

      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.oldRotation = new THREE.Euler().fromArray(json.oldRotation);
      this.newRotation = new THREE.Euler().fromArray(json.newRotation);

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
