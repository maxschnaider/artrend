/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class MultiCmdsCommand {

  constructor (editor) {

    this.type = 'MultiCmdsCommand';
    this.name = 'Multiple Changes';

    this.init = function (cmdArray) {

      this.cmdArray = ( cmdArray !== undefined ) ? cmdArray : [];

      return this.clone();

    };

    this.execute = function () {

      //  editor.signals.sceneGraphChanged.active = false;

      for (var i = 0; i < this.cmdArray.length; i++) {
        this.cmdArray[i].execute();
      }

      //  editor.signals.sceneGraphChanged.active = true;
      //  editor.signals.sceneGraphChanged.dispatch();

    };

    this.undo = function () {

      //  editor.signals.sceneGraphChanged.active = false;

      for (var i = this.cmdArray.length - 1; i >= 0; i--) {
        this.cmdArray[i].undo();
      }

      //  editor.signals.sceneGraphChanged.active = true;
      //  editor.signals.sceneGraphChanged.dispatch();

    };

    this.toJSON = function () {

      var output = Command.prototype.toJSON.call(this);
      var cmds = [];

      for (var i = 0; i < this.cmdArray.length; i++) {
        cmds.push(this.cmdArray[i].toJSON());
      }

      output.cmds = cmds;

      return output;

    };

    this.fromJSON = function (json) {

       editor.commander.fromJSON(this, json);
      var cmds = json.cmds;

      for (var i = 0; i < cmds.length; i++) {

        var cmd = new window[cmds[i].type]();	// creates a new object of type "json.type"
        cmd.fromJSON(cmds[i]);
        this.cmdArray.push(cmd);

      }

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
