/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class AddScriptCommand {

  constructor (editor) {

    this.type = 'AddScriptCommand';
    this.object = null;
    this.script = null;
    this.name = 'Add Script';

    this.init = function (object, script) {

      this.object = object;
      this.script = script;

      return this.clone();

    };

    this.execute = function () {

        if ( editor.scripts[this.object.uuid] === undefined) {
           editor.scripts[this.object.uuid] = [];
        }

         editor.scripts[this.object.uuid].push(this.script);

    };

    this.undo = function () {

      if ( editor.scripts[this.object.uuid] === undefined) return;
      let index =  editor.scripts[this.object.uuid].indexOf(this.script);

      if (index !== -1) {
         editor.scripts[this.object.uuid].splice(index, 1);
      }

    };

    this.toJSON = function () {

      let output =  editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.script = this.script;

      return output;

    };

    this.fromJSON = function (json) {

       editor.commander.fromJSON(this, json);

      this.script = json.script;
      this.object =  editor.objectByUuid(json.objectUuid);

    };

    this.clone = function () {

      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;

    };

  }
}
