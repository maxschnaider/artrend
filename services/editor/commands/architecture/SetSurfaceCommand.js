/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetSurfaceCommand {
  constructor (editor) {
    this.type = 'SetSurfaceCommand'
    this.name = 'Set Surface'

    this.init = function (surfaceId) {
      this.surfaceId = surfaceId
      this.selectedMaterialSlot = editor.selectedMaterialSlot

      let selectedSurfaceBySlot = editor.modelData.surfaceBySlot.filter(x => x.slotId === this.selectedMaterialSlot)[0]
      if (selectedSurfaceBySlot !== undefined) {
        this.oldSurfaceId = selectedSurfaceBySlot.surfaceId
      } else {
        this.oldSurfaceId = null
      }

      return this.clone()
    };

    this.execute = function () {
      if (editor.modelData.surfaceBySlot === undefined) {
        editor.modelData.surfaceBySlot = []
      }
      let surfaceBySlot = editor.modelData.surfaceBySlot.filter(surfBySlot => surfBySlot.slotId !== this.selectedMaterialSlot)
      surfaceBySlot.push({
        slotId: this.selectedMaterialSlot,
        surfaceId: this.surfaceId
      })
      editor.modelData.surfaceBySlot = surfaceBySlot
      editor.select(null)
      // editor.signals.sceneGraphChanged.dispatch();
    }

    this.undo = function () {
      this.surfaceBySlot[this.selectedMaterialSlot] = this.oldSurfaceId;
      // editor.signals.sceneGraphChanged.dispatch();
    };

    this.toJSON = function () {
      var output = editor.commander.toJSON(this);

      output.objectUuid = this.object.uuid;
      output.oldMaterial = this.oldMaterial.toJSON();
      output.newMaterial = this.newMaterial.toJSON();

      return output;
    };

    this.fromJSON = function (json) {
      editor.commander.fromJSON(this, json);

      this.object = editor.objectByUuid(json.objectUuid);
      this.oldMaterial = parseMaterial(json.oldMaterial);
      this.newMaterial = parseMaterial(json.newMaterial);

      function parseMaterial (json) {
        let loader = new THREE.ObjectLoader();
        let images = loader.parseImages(json.images);
        let textures  = loader.parseTextures(json.textures, images);
        let materials = loader.parseMaterials([json], textures);
        return materials[json.uuid];
      }
    };

    this.clone = function () {
      let newObject = Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      );

      return newObject;
    };

  }
}
