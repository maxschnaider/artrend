/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import Project from '../../../../models/Project'

export default class SetProjectByIdCommand {
  constructor(editor) {
    this.type = 'SetProjectByIdCommand'
    this.name = 'Set Project'

    this.init = function (projectId) {
      this.projectId = projectId
      return this.clone()
    }

    this.execute = function () {
      return new Promise(async resolve => {
        const project = await editor.ctx.app.$api.projects.getById(this.projectId)
        if (project instanceof Project) {
          editor.projectData = project.jsonData()
          const setSceneCmdRes = await editor.commander.setSceneByIdCommand.init(project.sceneId).execute()
          if (setSceneCmdRes.error) {
            resolve({ error: setSceneCmdRes.error })
          } else {
            resolve({ success: true })
          }
        } else {
          resolve({ error: 'Project fetching API error' })
        }
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      let newObject = Object.assign(Object.create(Object.getPrototypeOf(this)), this)
      return newObject
    }
  }
}
