/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

export default class SetLightCommand {
  constructor(editor) {
    this.type = 'SetLightCommand'
    this.name = 'Set Light'

    this.init = function (lightSettings) {
      this.lightSettings = lightSettings
      return this.clone()
    };

    this.execute = async function () {
      // editor.trend = JSON.parse(JSON.stringify(this.trend))
      // for (let surfBySlotNum in editor.model.surfaceBySlot) {
      //   let surfBySlot = editor.model.surfaceBySlot[surfBySlotNum]
      //   let materialSlot = surfBySlot.slotId
      //   let surfaceId = surfBySlot.surfaceId
      //   let materialBySurface = editor.trend.materialBySurface.filter(matBySurf => matBySurf.surfaceId === surfaceId)[0]
      //   if (materialBySurface !== undefined && materialBySurface.materialId !== undefined) {
      //     editor.execute(editor.commander.setMaterialByIdCommand.init(materialBySurface.materialId, materialSlot))
      //   }
      // }
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      return Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      )
    }
  }
}
