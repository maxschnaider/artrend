/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Scene from "../../../../models/Scene"

export default class SaveSceneCommand {
  constructor(editor) {
    this.type = 'SaveSceneCommand'
    this.name = 'Save scene'

    this.init = function () {
      if (editor.sceneData._id) {
        this.scene = new Scene(editor.sceneData)
      } else {
        this.scene = new Scene({
          modelId: editor.sceneData.modelId || editor.modelData._id,
          trendId: editor.sceneData.trendId || editor.trendData._id,
          // environmentId: editor.sceneData.environmentId || editor.environmentData._id
        })
      }
      return this.clone()
    }

    this.execute = function () {
      return new Promise(async resolve => {
        const res = await this.scene.save(editor.ctx)
        if (res.error || !(res.scene instanceof Scene)) {
          resolve({ error: res.error || 'Scene save error' })
        }
        editor.sceneData = res.scene.jsonData()
        resolve({
          success: true,
          scene: res.scene
        })
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      return Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      )
    }
  }
}
