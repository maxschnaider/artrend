/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import Trend from "../../../../models/Trend"

export default class SetTrendCommand {
  constructor(editor) {
    this.type = 'SetTrendCommand'
    this.name = 'Trending'

    this.init = function (trend) {
      this.trend = trend instanceof Trend ? trend : new Trend(trend)
      return this.clone()
    };

    this.execute = async function () {
      return new Promise(async resolve => {
        // if (editor.loader.startLoading(editor.modelData.surfaceBySlot) === false) {
        //   return
        // }
        editor.trendData = this.trend.jsonData()
        editor.sceneData.trendId = this.trend._id
        if (editor.trendData.materialBySurface !== undefined) {
          for (let surfBySlotNum in editor.modelData.surfaceBySlot) {
            const surfBySlot = editor.modelData.surfaceBySlot[surfBySlotNum]
            const materialSlot = surfBySlot.slotId
            const surfaceId = surfBySlot.surfaceId
            const materialBySurface = editor.trendData.materialBySurface.filter(matBySurf => matBySurf.surfaceId === surfaceId)[0]
            if (materialBySurface !== undefined && materialBySurface.materialId !== undefined) {
              editor.execute(editor.commander.setMaterialByIdCommand.init(materialBySurface.materialId, materialSlot))
            }
          }
          resolve({
            success: true,
            trend: this.trend
          })
        } else {
          resolve({ error: 'Data is not a trend' })
        }
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      return Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      )
    }
  }
}
