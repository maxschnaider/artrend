/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import Scene from '../../../../models/Scene'

export default class SetSceneByIdCommand {
  constructor(editor) {
    this.type = 'SetSceneByIdCommand'
    this.name = 'Set Scene'

    this.init = function (projectId) {
      this.projectId = projectId
      return this.clone()
    }

    this.execute = function () {
      return new Promise(async resolve => {
        editor.clear()
        const scene = await editor.ctx.app.$api.scenes.getById(this.projectId)
        if (scene instanceof Scene) {
          editor.sceneData = scene.jsonData()
          // const setEnvironmentCmdRes = await editor.commander.setEnvironmentCommand.init(scene.environmentId).execute()
          // if (setEnvironmentCmdRes.error) {
          //   resolve({ error: setEnvironmentCmdRes.error })
          // }
          const setModelCmdRes = await editor.commander.setModelByIdCommand.init(scene.modelId).execute()
          if (setModelCmdRes.error) {
            resolve({ error: setModelCmdRes.error })
          }
          const setTrendCmdRes = await editor.commander.setTrendByIdCommand.init(scene.trendId).execute()
          if (setTrendCmdRes.error) {
            resolve({ error: setTrendCmdRes.error })
          }
          resolve({ success: true })
        } else {
          resolve({ error: scene.error || 'Scene fetching API error' })
        }
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      let newObject = Object.assign(Object.create(Object.getPrototypeOf(this)), this)
      return newObject
    }
  }
}
