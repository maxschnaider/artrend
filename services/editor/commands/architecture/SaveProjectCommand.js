/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Project from "../../../../models/Project"
import Scene from "../../../../models/Scene"

export default class SaveProjectCommand {
  constructor(editor) {
    this.type = 'SaveProjectCommand'
    this.name = 'Save project'

    this.init = function () {
      if (editor.projectData._id) {
        this.project = new Project(editor.projectData)
      } else {
        this.project = new Project({
          name: `Project ${editor.ctx.app.store.state.projects.commonProjectsList.length + 1}`,
          authorId: editor.ctx.app.store.state.auth.user._id,
        })
      }
      return this.clone()
    }

    this.execute = function () {
      return new Promise(async resolve => {
        const saveSceneRes = await editor.commander.saveSceneCommand.init().execute()
        if (saveSceneRes.error || !(saveSceneRes.scene instanceof Scene)) {
          resolve({ error: saveSceneRes.error || 'Scene save error' })
        }
        this.project.sceneId = saveSceneRes.scene._id
        const saveProjectRes = await this.project.save(editor.ctx)
        if (!saveProjectRes.error && saveProjectRes.project instanceof Project) {
          resolve({
            success: true,
            project: saveProjectRes.project
          })
        } else {
          resolve({ error: saveProjectRes.error || 'Project save error' })
        }
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      return Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      )
    }
  }
}
