/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import Model from "../../../../models/Model"

export default class SetModelByIdCommand {

  constructor(editor) {
    this.type = 'SetModelByIdCommand'
    this.name = 'Set Model'

    this.init = function (modelId) {
      this.modelId = modelId
      return this.clone()
    };

    this.execute = function () {
      return new Promise(async resolve => {
        let objects = editor.scene.children
        while (objects.length > 0) {
          editor.removeObject(objects[0])
        }
        editor.setLight()

        const model = await editor.ctx.app.$api.models.getById(this.modelId)
        if (model instanceof Model) {
          const loadModelRes = await editor.loader.loadFBX(model.fileUrl)
          if (loadModelRes.success) {
            editor.modelData = model.jsonData()
            editor.sceneData.modelId = model._id
            resolve({
              success: true,
              model: model
            })
          } else {
            resolve({ error: model.fileUrl === undefined ? 'Model have no fileUrl' : 'Load FBX error' })
          }
        } else {
          resolve({ error: 'Model fetching API error' })
        }
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      return Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      )
    }
  }
}
