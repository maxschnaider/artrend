/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import Material from '../../../../models/Material'

export default class SetMaterialByIdCommand {

  constructor(editor) {
    this.type = 'SetMaterialByIdCommand'
    this.name = 'Set Material'

    this.init = function (materialId, materialSlot) {
      this.object = editor.modelMesh
      this.materialId = materialId
      this.materialSlot = materialSlot !== undefined ? materialSlot : editor.selectedMaterialSlot
      editor.deselectMaterial()
      return this.clone()
    }

    this.execute = function () {
      return new Promise(async resolve => {
        let self = this
        let surfaceBySlot = editor.modelData.surfaceBySlot.filter(x => x.slotId === this.materialSlot)[0]
        if (surfaceBySlot !== undefined) {
          let materialBySurface = editor.trendData.materialBySurface.filter(matBySurf => matBySurf.surfaceId !== surfaceBySlot.surfaceId)
          materialBySurface.push({
            materialId: this.materialId,
            surfaceId: surfaceBySlot.surfaceId
          })
          editor.trendData.materialBySurface = materialBySurface
        }

        const materialObj = await editor.ctx.app.$api.materials.getById(this.materialId)
        if (!(materialObj instanceof Material)) {
          resolve({ error: 'Material fetching API error' })
        }

        if (materialObj.thumbnailColor && materialObj.filePath !== undefined) {
          let thumbnailMaterial = new THREE.MeshPhongMaterial({ color: materialObj.thumbnailColor })
          editor.commander.setMaterialCommand.init(self.object, self.materialSlot, thumbnailMaterial).execute()
        }

        if (materialObj.filePath === undefined) {
          let material = new THREE.MeshStandardMaterial({
            roughness: materialObj.settings.roughness,
            metalness: materialObj.settings.metalness,
            opacity: materialObj.settings.opacity,
            transparent: materialObj.settings.opacity !== 1,
            color: materialObj.settings.color,
            envMap: editor.envMap
          })
          editor.execute(editor.commander.setMaterialCommand.init(self.object, self.materialSlot, material))
          resolve({ success: true })
        } else {
          const url = materialObj.fileUrl(editor.config.textureSizeName, editor.config.jpegQuality)
          editor.loader.loadTexture(url, function (texture) {
            if (!texture) { return }
            texture.wrapS = THREE.RepeatWrapping
            texture.wrapT = THREE.RepeatWrapping
            texture.rotation = materialObj.settings.rotation
            texture.repeat.set(materialObj.settings.scaleX, materialObj.settings.scaleY)
            texture.needUpdate = true

            let material = new THREE.MeshStandardMaterial({
              map: texture,
              roughness: materialObj.settings.roughness,
              metalness: materialObj.settings.metalness,
              envMap: editor.envMap
            })

            // BUMP
            // const urlBump = '../editor/assets/textures/trends/' + trend + '/' + materialId + 'b.jpg'
            //
            // editor.loadTexture(urlBump, function (textureBump, success) {
            //     if (success) {
            //         textureBump.wrapS = THREE.RepeatWrapping
            //         textureBump.wrapT = THREE.RepeatWrapping
            //         if (surfaceId === 'RoofTile') {
            //             textureBump.repeat.set(0.2, 0.2)
            //         } else {
            //             // bMap.repeat.set(0.2, 0.2)
            //             textureBump.repeat.set(0.05, 0.05)
            //         }
            //         textureBump.needUpdate = true
            //
            //         loadedMaterial = new THREE.MeshStandardMaterial({
            //             map: texture,
            //             bumpMap: textureBump,
            //             // bumpScale: 0.3,
            //             // reflectivity: 0.1,
            //             // shininess: 0.1,
            //             // emissiveIntensity: 0.2,
            //             // combine: THREE.MixOperation,
            //             envMap: editor.envMap
            //         })
            //     } else {
            //         loadedMaterial = new THREE.MeshStandardMaterial({
            //             map: texture,
            //             envMap: editor.envMap
            //         })
            //     }
            //
            //     editor.loadedURLs[url] = loadedMaterial
            //     editor.applyMaterial(objectUuid, materialSlot, loadedMaterial)
            // })

            editor.execute(editor.commander.setMaterialCommand.init(self.object, self.materialSlot, material))
            resolve({ success: true })
          })
        }
        // editor.signals.sceneGraphChanged.dispatch()
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      let newObject = Object.assign(Object.create(Object.getPrototypeOf(this)), this)
      return newObject
    }
  }
}
