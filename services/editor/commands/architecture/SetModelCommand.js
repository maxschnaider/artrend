/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import Model from "../../../../models/Model"

export default class SetModelCommand {
  constructor(editor) {
    this.type = 'SetModelCommand'
    this.name = 'Set Model'

    this.init = function (model) {
      this.model = model instanceof Model ? model : new Model(model)
      return this.clone()
    };

    this.execute = async function () {
      return new Promise(async resolve => {
        let objects = editor.scene.children
        while (objects.length > 0) {
          editor.removeObject(objects[0])
        }
        editor.setLight()

        const loadModelRes = await editor.loader.loadFBX(this.model.fileUrl)
        if (loadModelRes.success) {
          editor.modelData = this.model.jsonData()
          editor.sceneData.modelId = this.model._id
          resolve({
            success: true,
            model: this.model
          })
        } else {
          resolve({ error: this.model.fileUrl === undefined ? 'Model have no fileUrl' : 'Load FBX error' })
        }
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      return Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      )
    }
  }
}
