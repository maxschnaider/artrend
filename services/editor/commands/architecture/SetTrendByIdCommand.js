/*
 * Project: artrend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import Trend from "../../../../models/Trend"

export default class SetTrendByIdCommand {
  constructor(editor) {
    this.type = 'SetTrendByIdCommand'
    this.name = 'Trending'

    this.init = function (trendId) {
      this.trendId = trendId
      return this.clone()
    };

    this.execute = function () {
      return new Promise(async resolve => {
        // if (editor.loader.startLoading(editor.modelData.surfaceBySlot) === false) {
        //   return
        // }
        const trend = await editor.ctx.app.$api.trends.getById(this.trendId)
        if (trend instanceof Trend) {
          editor.trendData = trend.jsonData()
          editor.sceneData.trendId = trend._id
          for (let surfBySlotNum in editor.modelData.surfaceBySlot) {
            const surfBySlot = editor.modelData.surfaceBySlot[surfBySlotNum]
            const materialSlot = surfBySlot.slotId
            const surfaceId = surfBySlot.surfaceId
            const materialBySurface = editor.trendData.materialBySurface.filter(matBySurf => matBySurf.surfaceId === surfaceId)[0]
            if (materialBySurface !== undefined && materialBySurface.materialId !== undefined) {
              editor.execute(editor.commander.setMaterialByIdCommand.init(materialBySurface.materialId, materialSlot))
            }
          }
          resolve({
            success: true,
            trend: trend
          })
        } else {
          resolve({ error: trend.error || 'Trend fetching API error' })
        }
      })
    }

    this.undo = function () {
    }

    this.toJSON = function () {
    }

    this.fromJSON = function (json) {
    }

    this.clone = function () {
      return Object.assign(
        Object.create(
          Object.getPrototypeOf(this)
        ),
        this
      )
    }
  }
}
