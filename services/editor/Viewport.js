/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'

// let OrbitControls = require('three-orbit-controls')(THREE)
require('three-editor-controls')(THREE)

export default class Viewport {

  constructor(editor) {
    let objects = []

    // helpers
    // let grid = new THREE.GridHelper(30, 30, 0x444444, 0x888888)
    // sceneHelpers.add(grid)
    // let array = grid.geometry.attributes.color.array
    //
    // for (var i = 0 i < array.length i += 60) {
    //
    //     for (var j = 0 j < 12 j ++) {
    //         array[i + j] = 0.26
    //     }
    // }
    //
    // let box = new THREE.Box3()
    //
    // let selectionBox = new THREE.BoxHelper()
    // selectionBox.material.depthTest = false
    // selectionBox.material.transparent = true
    // selectionBox.visible = false
    // sceneHelpers.add(selectionBox)

    // let objectPositionOnDown = null
    // let objectRotationOnDown = null
    // let objectScaleOnDown = null

    // let transformControls = new THREE.TransformControls(camera, editor.domElement)
    // transformControls.addEventListener('change', function () {
    //   var object = transformControls.object
    //
    //   if (object !== undefined) {
    //     selectionBox.setFromObject(object)
    //
    //     if (editor.helpers[object.id] !== undefined) {
    //       editor.helpers[object.id].update()
    //     }
    //     signals.refreshSidebarObject3D.dispatch(object)
    //   }
    //   render()
    // })
    // transformControls.addEventListener('mouseDown', function () {
    //
    //   var object = transformControls.object
    //
    //   objectPositionOnDown = object.position.clone()
    //   objectRotationOnDown = object.rotation.clone()
    //   objectScaleOnDown = object.scale.clone()
    //
    //   controls.enabled = false
    // })
    // transformControls.addEventListener('mouseUp', function () {
    //   var object = transformControls.object
    //
    //   if (object !== undefined) {
    //     switch (transformControls.getMode()) {
    //       case 'translate':
    //         if (!objectPositionOnDown.equals(object.position)) {
    //           editor.execute(new SetPositionCommand(object, object.position, objectPositionOnDown))
    //         }
    //         break
    //       case 'rotate':
    //         if (!objectRotationOnDown.equals(object.rotation)) {
    //           editor.execute(new SetRotationCommand(object, object.rotation, objectRotationOnDown))
    //         }
    //         break
    //       case 'scale':
    //         if (!objectScaleOnDown.equals(object.scale)) {
    //           editor.execute(new SetScaleCommand(object, object.scale, objectScaleOnDown))
    //         }
    //         break
    //     }
    //   }
    //
    //   controls.enabled = true
    // })
    //
    // sceneHelpers.add(transformControls)

    // object picking

    let raycaster = new THREE.Raycaster()
    let mouse = new THREE.Vector2()

    // events

    function getIntersects(point, objects) {
      mouse.set((point.x * 2) - 1, -(point.y * 2) + 1)
      raycaster.setFromCamera(mouse, editor.camera)
      return raycaster.intersectObjects(objects)
    }

    let onDownPosition = new THREE.Vector2()
    let onUpPosition = new THREE.Vector2()
    let onDoubleClickPosition = new THREE.Vector2()

    function getMousePosition(dom, x, y) {
      let rect = dom.getBoundingClientRect()
      return [(x - rect.left) / rect.width, (y - rect.top) / rect.height]
    }

    function handleClick() {
      if (onDownPosition.distanceTo(onUpPosition) === 0) {
        let intersects = getIntersects(onUpPosition, objects)

        if (intersects.length > 0) {
          let object = intersects[0].object
          editor.selectMaterial(object, intersects[0].face.materialIndex)

          if (editor.trendData._id !== undefined && editor.showMaterialsPanel !== undefined) {
            editor.showMaterialsPanel(true)
          } else if (editor.showSurfacesPanel !== undefined) {
            editor.showSurfacesPanel(true)
          }

        } else {
          editor.select(null)

          if (editor.trendData._id !== undefined && editor.showMaterialsPanel !== undefined) {
            editor.showMaterialsPanel(false)
          } else if (editor.showSurfacesPanel !== undefined) {
            editor.showSurfacesPanel(false)
          }
        }

        render()
      }
    }

    function onMouseDown(event) {
      event.preventDefault()

      let array = getMousePosition(editor.domElement, event.clientX, event.clientY)
      onDownPosition.fromArray(array)

      document.addEventListener('mouseup', onMouseUp, false)
    }

    function onMouseUp(event) {
      let array = getMousePosition(editor.domElement, event.clientX, event.clientY)
      onUpPosition.fromArray(array)

      handleClick()

      document.removeEventListener('mouseup', onMouseUp, false)
    }

    function onTouchStart(event) {
      let touch = event.changedTouches[0]
      let array = getMousePosition(editor.domElement, touch.clientX, touch.clientY)
      onDownPosition.fromArray(array)

      document.addEventListener('touchend', onTouchEnd, false)
    }

    function onTouchEnd(event) {
      let touch = event.changedTouches[0]
      let array = getMousePosition(editor.domElement, touch.clientX, touch.clientY)
      onUpPosition.fromArray(array)

      handleClick()

      document.removeEventListener('touchend', onTouchEnd, false)
    }

    function onDoubleClick(event) {
      let array = getMousePosition(editor.domElement, event.clientX, event.clientY)
      onDoubleClickPosition.fromArray(array)

      let intersects = getIntersects(onDoubleClickPosition, objects)

      if (intersects.length > 0) {
        let intersect = intersects[0]
        editor.viewport.objectFocused(intersect.object)
      }
    }

    editor.domElement.addEventListener('mousedown', onMouseDown, false)
    editor.domElement.addEventListener('touchstart', onTouchStart, false)
    editor.domElement.addEventListener('dblclick', onDoubleClick, false)

    let controls = new THREE.EditorControls(editor.camera, editor.domElement)

    controls.addEventListener('change', function () {
      editor.viewport.cameraChanged(editor.camera)
    })

    // let orbitControls = new OrbitControls(editor.camera, editor.domElement)

    this.editorCleared = function () {
      controls.center.set(0, 0, 0)
      render()
    }

    // this.transformModeChanged = function (mode) {
    //   transformControls.setMode(mode)
    // }
    //
    //this.snapChanged = function (dist) {
    //   transformControls.setTranslationSnap(dist)
    // }
    //
    //this.spaceChanged = function (space) {
    //   transformControls.setSpace(space)
    // }

    this.rendererChanged = function (newRenderer) {
      if (editor.renderer !== null) {
        editor.domElement.removeChild(editor.renderer.domElement)
      }

      editor.renderer = newRenderer
      editor.renderer.autoClear = false
      editor.renderer.autoUpdateScene = false
      editor.renderer.setPixelRatio(window.devicePixelRatio)
      editor.renderer.setSize(editor.domElement.offsetWidth, editor.domElement.offsetHeight)
      editor.domElement.appendChild(editor.renderer.domElement)

      render()
    }

    this.sceneGraphChanged = function () {
      render()
    }

    this.cameraChanged = function () {
      render()
    }

    this.objectSelected = function (object) {
      // selectionBox.visible = false
      // transformControls.detach()
      //
      // if (object !== null && object !== scene && object !== camera) {

      // box.setFromObject(object)
      //
      // if (box.isEmpty() === false) {
      //
      //     selectionBox.setFromObject(object)
      //     selectionBox.visible = true
      //
      // }
      //
      // transformControls.attach(object)


      // var boundingSphereRadius = new THREE.Box3().setFromObject(object)
      //     .getBoundingSphere()
      //     .radius
      // orbitControls.minDistance = boundingSphereRadius / 2
      // orbitControls.maxDistance = boundingSphereRadius * 2
      //     render()
      // }

      render()
    }

    this.objectFocused = function (object) {
      controls.focus(object)
    }

    this.geometryChanged = function (object) {
      // if (object !== undefined) {
      //   selectionBox.setFromObject(object)
      // }

      render()
    }

    this.objectAdded = function (object) {
      object.traverse(function (child) {
        objects.push(child)
      })
    }

    this.objectChanged = function (object) {
      // if (editor.selected === object) {
      //   selectionBox.setFromObject(object)
      // }

      if (object instanceof THREE.PerspectiveCamera) {
        object.updateProjectionMatrix()
      }

      if (editor.helpers[object.id] !== undefined) {
        editor.helpers[object.id].update()
      }

      render()
    }

    this.objectRemoved = function (object) {
      // if (object === transformControls.object) {
      //   transformControls.detach()
      // }

      object.traverse(function (child) {
        objects.splice(objects.indexOf(child), 1)
      })
    }

    this.helperAdded = function (object) {
      objects.push(object.getObjectByName('picker'))
    }

    this.helperRemoved = function (object) {
      objects.splice(objects.indexOf(object.getObjectByName('picker')), 1)
    }

    this.materialChanged = function (material) {
      render()
    }

    this.sceneBackgroundChanged = function (backgroundColor) {
      editor.scene.background.setHex(backgroundColor)
      render()
    }

    let currentFogType = null

    this.sceneFogChanged = function (fogType, fogColor, fogNear, fogFar, fogDensity) {
      if (currentFogType !== fogType) {
        switch (fogType) {
          case 'None':
            editor.scene.fog = null
            break
          case 'Fog':
            editor.scene.fog = new THREE.Fog()
            break
          case 'FogExp2':
            editor.scene.fog = new THREE.FogExp2()
            break
        }
        currentFogType = fogType
      }

      if (editor.scene.fog instanceof THREE.Fog) {
        editor.scene.fog.color.setHex(fogColor)
        editor.scene.fog.near = fogNear
        editor.scene.fog.far = fogFar
      } else if (editor.scene.fog instanceof THREE.FogExp2) {
        editor.scene.fog.color.setHex(fogColor)
        editor.scene.fog.density = fogDensity
      }
      render()
    }

    this.windowResize = function () {
      editor.DEFAULT_CAMERA.aspect = editor.domElement.clientWidth / editor.domElement.clientHeight
      editor.DEFAULT_CAMERA.updateProjectionMatrix()

      editor.camera.aspect = editor.domElement.clientWidth / editor.domElement.clientHeight
      editor.camera.updateProjectionMatrix()

      editor.renderer.setSize(editor.domElement.clientWidth, editor.domElement.clientHeight)

      render()
    }

    // this.showGridChanged = function (showGrid) {
    //
    //   grid.visible = showGrid
    //   render()
    //
    // }

    //

    function render() {
      editor.sceneHelpers.updateMatrixWorld()
      editor.scene.updateMatrixWorld()
      // editor.sightLight.position.copy(camera.position)

      editor.renderer.render(editor.scene, editor.camera)

      // if (editor.renderer instanceof THREE.RaytracingRenderer === false) {
      //   renderer.render(sceneHelpers, camera)
      // }
    }

    this.animate = function () {
      if (editor.config.animate) {
        requestAnimationFrame(editor.viewport.animate)
        render()
      }
    }

  }
}
