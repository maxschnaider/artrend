/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

export default class Config {
  constructor() {
    this.animate = false
    this.autosave = false
    this.theme = 'css/dark.css'

    // scene
    this.sceneBackgroundColor = 0xffffff

    // project
    this.projectTitle = ''
    this.projectEditable = false

    // renderer
    this.renderer = 'WebGLRenderer'
    this.rendererAntialias = true
    this.rendererGammaInput = false
    this.rendererGammaOutput = false
    this.rendererShadows = true

    this.VR = false

    this.history = false

    this.shortcutsTranslate = 'w'
    this.shortcutsRotate = 'e'
    this.shortcutsScale = 'r'
    this.shortcutsUndo = 'z'
    this.shortcutsFocus = 'f'

    // quality
    this.textureSizeName = "medium"
    this.jpegQuality = 40
  }
}
