/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import AddObjectCommand from './commands/AddObjectCommand'
import AddScriptCommand from './commands/AddScriptCommand'
import MoveObjectCommand from './commands/MoveObjectCommand'
import MultiCmdsCommand from './commands/MultiCmdsCommand'
import RemoveObjectCommand from './commands/RemoveObjectCommand'
import RemoveScriptCommand from './commands/RemoveScriptCommand'
import SetColorCommand from './commands/SetColorCommand'
import SetGeometryCommand from './commands/SetGeometryCommand'
import SetGeometryValueCommand from './commands/SetGeometryValueCommand'
import SetMaterialColorCommand from './commands/SetMaterialColorCommand'
import SetMaterialCommand from './commands/SetMaterialCommand'
import SetMaterialMapCommand from './commands/SetMaterialMapCommand'
import SetMaterialValueCommand from './commands/SetMaterialValueCommand'
import SetPositionCommand from './commands/SetPositionCommand'
import SetRotationCommand from './commands/SetRotationCommand'
import SetScaleCommand from './commands/SetScaleCommand'
import SetSceneCommand from './commands/SetSceneCommand'
import SetScriptValueCommand from './commands/SetScriptValueCommand'
import SetUuidCommand from './commands/SetUuidCommand'
import SetValueCommand from './commands/SetValueCommand'

import SetSurfaceCommand from './commands/architecture/SetSurfaceCommand'
import SetTrendCommand from './commands/architecture/SetTrendCommand'
import SetTrendByIdCommand from './commands/architecture/SetTrendByIdCommand'
import SetMaterialByIdCommand from './commands/architecture/SetMaterialByIdCommand'
import SetProjectByIdCommand from './commands/architecture/SetProjectByIdCommand'
import SetLightCommand from './commands/architecture/SetLightCommand'
import SetEnvironmentCommand from './commands/architecture/SetEnvironmentCommand'
import SetModelByIdCommand from './commands/architecture/SetModelByIdCommand'
import SetModelCommand from './commands/architecture/SetModelCommand'
import SetSceneByIdCommand from './commands/architecture/SetSceneByIdCommand'

import SaveSceneCommand from './commands/architecture/SaveSceneCommand'
import SaveProjectCommand from './commands/architecture/SaveProjectCommand'

export default class Commander {
  constructor(editor) {
    this.addObjectCommand = new AddObjectCommand(editor)
    this.addScriptCommand = new AddScriptCommand(editor)
    this.moveObjectCommand = new MoveObjectCommand(editor)
    this.multiCmdsCommand = new MultiCmdsCommand(editor)
    this.removeObjectCommand = new RemoveObjectCommand(editor)
    this.removeScriptCommand = new RemoveScriptCommand(editor)
    this.setColorCommand = new SetColorCommand(editor)
    this.setGeometryCommand = new SetGeometryCommand(editor)
    this.setGeometryValueCommand = new SetGeometryValueCommand(editor)
    this.setMaterialColorCommand = new SetMaterialColorCommand(editor)
    this.setMaterialCommand = new SetMaterialCommand(editor)
    this.setMaterialMapCommand = new SetMaterialMapCommand(editor)
    this.setMaterialValueCommand = new SetMaterialValueCommand(editor)
    this.setPositionCommand = new SetPositionCommand(editor)
    this.setRotationCommand = new SetRotationCommand(editor)
    this.setScaleCommand = new SetScaleCommand(editor)
    this.setSceneCommand = new SetSceneCommand(editor)
    this.setScriptValueCommand = new SetScriptValueCommand(editor)
    this.setUuidCommand = new SetUuidCommand(editor)
    this.setValueCommand = new SetValueCommand(editor)

    // architecture logic
    this.setSurfaceCommand = new SetSurfaceCommand(editor)
    this.setTrendCommand = new SetTrendCommand(editor)
    this.setTrendByIdCommand = new SetTrendByIdCommand(editor)
    this.setMaterialByIdCommand = new SetMaterialByIdCommand(editor)
    this.setProjectByIdCommand = new SetProjectByIdCommand(editor)
    this.setLightCommand = new SetLightCommand(editor)
    this.setEnvironmentCommand = new SetEnvironmentCommand(editor)
    this.setModelByIdCommand = new SetModelByIdCommand(editor)
    this.setModelCommand = new SetModelCommand(editor)
    this.setSceneByIdCommand = new SetSceneByIdCommand(editor)

    this.saveSceneCommand = new SaveSceneCommand(editor)
    this.saveProjectCommand = new SaveProjectCommand(editor)
  }
}
