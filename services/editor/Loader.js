/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import * as THREE from 'three'
import * as FBXLoader from 'three-fbxloader-offical'

export default class Loader {

  constructor (editor) {

    let scope = this;
    this.texturePath = '/assets/textures';

    let textureLoader = new THREE.TextureLoader();
    let cubeTextureLoader = new THREE.CubeTextureLoader();
    this.loadedURLs = {};

    var isLoading = false;
    var loadingCounter = 0;
    var totalLoadingCount = 0;

    this.startLoading = function (loadedObject) {
      countLoadingObjects(loadedObject);
      return !(isLoading === true || totalLoadingCount === 0)
    };

    function countLoadingObjects (loadedObject) {
      if (typeof loadedObject === "undefined") {
        totalLoadingCount = 1;
        return;
      }

      for (const childKey in loadedObject) {
        for (const childValue in loadedObject[childKey]) {
          totalLoadingCount += 1;
        }
      }
    }

    this.updateLoading = function () {
      loadingCounter += 1;
    };

    this.loadTexture = function (url, completion) {
      textureLoader.load(url,
        // onLoad callback
        function (texture) {
          scope.updateLoading();
          completion(texture, true);
        },

        undefined,

        function (err) {
          scope.updateLoading();
          console.error('Texture is missing: ' + url);
          completion(null, false);
        }
      );
    };

    this.loadCubeTexture = function (urls, completion) {
      cubeTextureLoader.load(urls,
        // onLoad callback
        function (cubeTexture) {
          scope.updateLoading()
          scope.loadedURLs[urls[0]] = cubeTexture
          completion(cubeTexture, true)
        },

        undefined,

        function (err) {
          scope.updateLoading();
          console.error('Texture is missing: ' + urls[0])
          completion(null, false)
        }
      )
    }

    function clear () {
      totalLoadingCount = 0;
      loadingCounter = 0;
    }

    this.loadFiles = function (files) {
      if (files.length > 0) {
        let filesMap = createFileMap(files);
        let manager = new THREE.LoadingManager();
        manager.setURLModifier(function (url) {
          let file = filesMap[url]
          if (file) {
            console.log('Loading', url)
            return URL.createObjectURL(file)
          }
          return url
        })
        for (var i = 0; i < files.length; i ++) {
          scope.loadFile(files[ i ], manager)
        }
      }
    }

    this.loadFBX = function (url) {
      return new Promise(resolve => {
        this.startLoading()
        var loader = new FBXLoader()

        loader.load(url, function (object) {
          scope.updateLoading()

          colorize(object)

          editor.execute(editor.commander.addObjectCommand.init(object))
          editor.execute(editor.commander.setPositionCommand.init(object, new THREE.Vector3(0, 0, 0)))
          editor.execute(editor.commander.setRotationCommand.init(object, new THREE.Euler(-90 * THREE.Math.DEG2RAD, 0, -90 * THREE.Math.DEG2RAD)))

          editor.modelMesh = object.children[0]
          resolve({
            success: true,
            mesh: object.children[0]
          })
        })
      })
    }

    this.loadFile = function (file, manager) {
      let filename = file.name;
      let extension = filename.split('.').pop().toLowerCase();
      let reader = new FileReader();
      reader.addEventListener('progress', function (event) {
        // var size = '(' + Math.floor(event.total / 1000).format() + ' KB)';
        // var progress = Math.floor((event.loaded / event.total) * 100) + '%';
        // console.log('Loading', filename, size, progress);
      });

      switch (extension) {

        case '3ds':

          reader.addEventListener('load', function (event) {

            var loader = new THREE.TDSLoader();
            var object = loader.parse(event.target.result);

            editor.execute(editor.commander.addObjectCommand.init(object));

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'amf':

          reader.addEventListener('load', function (event) {

            var loader = new THREE.AMFLoader();
            var amfobject = loader.parse(event.target.result);

            editor.execute(editor.commander.addObjectCommand.init(amfobject));

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'awd':

          reader.addEventListener('load', function (event) {

            var loader = new THREE.AWDLoader();
            var scene = loader.parse(event.target.result);

            editor.execute(editor.commander.setSceneCommand.init(scene));

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'babylon':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;
            var json = JSON.parse(contents);

            var loader = new THREE.BabylonLoader();
            var scene = loader.parse(json);

            editor.execute(editor.commander.setSceneCommand.init(scene));

          }, false);
          reader.readAsText(file);

          break;

        case 'babylonmeshdata':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;
            var json = JSON.parse(contents);

            var loader = new THREE.BabylonLoader();

            var geometry = loader.parseGeometry(json);
            var material = new THREE.MeshStandardMaterial();

            var mesh = new THREE.Mesh(geometry, material);
            mesh.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(mesh));

          }, false);
          reader.readAsText(file);

          break;

        case 'ctm':

          reader.addEventListener('load', function (event) {

            var data = new Uint8Array(event.target.result);

            var stream = new CTM.Stream(data);
            stream.offset = 0;

            var loader = new THREE.CTMLoader();
            loader.createModel(new CTM.File(stream), function(geometry) {

              geometry.sourceType = "ctm";
              geometry.sourceFile = file.name;

              var material = new THREE.MeshStandardMaterial();

              var mesh = new THREE.Mesh(geometry, material);
              mesh.name = filename;

              editor.execute(editor.commander.addObjectCommand.init(mesh));

            });

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'dae':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var loader = new THREE.ColladaLoader(manager);
            var collada = loader.parse(contents);

            collada.scene.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(collada.scene));

          }, false);
          reader.readAsText(file);

          break;

        case 'fbx':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;
            var loader = new FBXLoader(manager);
            var object = loader.parse(contents);

            colorize(object);

            editor.execute(editor.commander.addObjectCommand.init(object));
            editor.execute(editor.commander.setPositionCommand.init(object, new THREE.Vector3(0, 0, 0)));
            editor.execute(editor.commander.setRotationCommand.init(object, new THREE.Euler(-90 * THREE.Math.DEG2RAD, 0, -90 * THREE.Math.DEG2RAD)));

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'glb':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var loader = new THREE.GLTFLoader();
            loader.parse(contents, '', function (result) {

              result.scene.name = filename;
              editor.execute(editor.commander.addObjectCommand.init(result.scene));

            });

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'gltf':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var loader;

            if (isGLTF1(contents)) {

              loader = new THREE.LegacyGLTFLoader(manager);

            } else {

              loader = new THREE.GLTFLoader(manager);

            }

            loader.parse(contents, '', function (result) {

              result.scene.name = filename;
              editor.execute(editor.commander.addObjectCommand.init(result.scene));

            });

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'js':
        case 'json':

        case '3geo':
        case '3mat':
        case '3obj':
        case '3scn':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            // 2.0

            if (contents.indexOf('postMessage') !== - 1) {

              var blob = new Blob([ contents ], { type: 'text/javascript' });
              var url = URL.createObjectURL(blob);

              var worker = new Worker(url);

              worker.onmessage = function (event) {

                event.data.metadata = { version: 2 };
                handleJSON(event.data, file, filename);

              };

              worker.postMessage(Date.now());

              return;

            }

            // >= 3.0

            var data;

            try {

              data = JSON.parse(contents);

            } catch (error) {

              alert(error);
              return;

            }

            handleJSON(data, file, filename);

          }, false);
          reader.readAsText(file);

          break;


        case 'kmz':

          reader.addEventListener('load', function (event) {

            var loader = new THREE.KMZLoader();
            var collada = loader.parse(event.target.result);

            collada.scene.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(collada.scene));

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'md2':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var geometry = new THREE.MD2Loader().parse(contents);
            var material = new THREE.MeshStandardMaterial({
              morphTargets: true,
              morphNormals: true
            });

            var mesh = new THREE.Mesh(geometry, material);
            mesh.mixer = new THREE.AnimationMixer(mesh);
            mesh.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(mesh));

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'obj':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var object = new THREE.OBJLoader().parse(contents);
            object.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(object));

          }, false);
          reader.readAsText(file);

          break;

        case 'playcanvas':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;
            var json = JSON.parse(contents);

            var loader = new THREE.PlayCanvasLoader();
            var object = loader.parse(json);

            editor.execute(editor.commander.addObjectCommand.init(object));

          }, false);
          reader.readAsText(file);

          break;

        case 'ply':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var geometry = new THREE.PLYLoader().parse(contents);
            geometry.sourceType = "ply";
            geometry.sourceFile = file.name;

            var material = new THREE.MeshStandardMaterial();

            var mesh = new THREE.Mesh(geometry, material);
            mesh.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(mesh));

          }, false);
          reader.readAsArrayBuffer(file);

          break;

        case 'stl':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var geometry = new THREE.STLLoader().parse(contents);
            geometry.sourceType = "stl";
            geometry.sourceFile = file.name;

            var material = new THREE.MeshStandardMaterial();

            var mesh = new THREE.Mesh(geometry, material);
            mesh.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(mesh));

          }, false);

          if (reader.readAsBinaryString !== undefined) {

            reader.readAsBinaryString(file);

          } else {

            reader.readAsArrayBuffer(file);

          }

          break;

        case 'svg':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var loader = new THREE.SVGLoader();
            var paths = loader.parse(contents);

            //

            var group = new THREE.Group();
            group.scale.multiplyScalar(0.1);
            group.scale.y *= -1;

            for (var i = 0; i < paths.length; i ++) {

              var path = paths[ i ];

              var material = new THREE.MeshBasicMaterial({
                color: path.color,
                depthWrite: false
              });

              var shapes = path.toShapes(true);

              for (var j = 0; j < shapes.length; j ++) {

                var shape = shapes[ j ];

                var geometry = new THREE.ShapeBufferGeometry(shape);
                var mesh = new THREE.Mesh(geometry, material);

                group.add(mesh);

              }

            }

            editor.execute(editor.commander.addObjectCommand.init(group));

          }, false);
          reader.readAsText(file);

          break;

        case 'vtk':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var geometry = new THREE.VTKLoader().parse(contents);
            geometry.sourceType = "vtk";
            geometry.sourceFile = file.name;

            var material = new THREE.MeshStandardMaterial();

            var mesh = new THREE.Mesh(geometry, material);
            mesh.name = filename;

            editor.execute(editor.commander.addObjectCommand.init(mesh));

          }, false);
          reader.readAsText(file);

          break;

        case 'wrl':

          reader.addEventListener('load', function (event) {

            var contents = event.target.result;

            var result = new THREE.VRMLLoader().parse(contents);

            editor.execute(editor.commander.setSceneCommand.init(result));

          }, false);
          reader.readAsText(file);

          break;

        case 'zip':
          reader.addEventListener('load', function (event) {
            handleZIP(event.target.result)
          }, false)
          reader.readAsBinaryString(file);
          break

        default:
          break;
      }
    }

    function colorize (object) {
      var colors = [
        0xb0b0b0,
        0xb1b1b1,
        0xb2b2b2,
        0xb3b3b3,
        0xb4b4b4,
        0xb5b5b5,
        0xb6b6b6,
        0xb7b7b7,
        0xb8b8b8,
        0xb9b9b9,
        0xbababa,
        0xbbbbbb,
        0xbcbcbc,
        0xbdbdbd,
        0xbebebe,
        0xbfbfbf]

      var lastAppliedColor
      var newColor

      object.traverse(function (child) {
        if (child.material) {
          child.geometry = new THREE.Geometry().fromBufferGeometry(child.geometry)
          child.castShadow = true
          child.receiveShadow = true
          for (var materialId = 0; materialId < child.material.length; materialId++) {
            do {
              newColor = colors[Math.floor(Math.random() * 16)]
            } while (newColor === lastAppliedColor)
            child.material[materialId] = new THREE.MeshPhongMaterial({ color: newColor })
          }
        }
      })
    }

    // function handleJSON(data, file, filename) {
    //
    //   if (data.metadata === undefined) { // 2.0
    //
    //     data.metadata = { type: 'Geometry' };
    //
    //   }
    //
    //   if (data.metadata.type === undefined) { // 3.0
    //
    //     data.metadata.type = 'Geometry';
    //
    //   }
    //
    //   if (data.metadata.formatVersion !== undefined) {
    //
    //     data.metadata.version = data.metadata.formatVersion;
    //
    //   }
    //
    //   switch (data.metadata.type.toLowerCase()) {
    //
    //     case 'buffergeometry':
    //
    //       let loader = new THREE.BufferGeometryLoader();
    //       let result = loader.parse(data);
    //
    //       let mesh = new THREE.Mesh(result);
    //
    //       editor.execute(editor.commander.addObjectCommand.init(mesh));
    //
    //       break;
    //
    //     case 'geometry':
    //
    //       var loader = new THREE.JSONLoader();
    //       loader.setTexturePath(scope.texturePath);
    //
    //       var result = loader.parse(data);
    //
    //       let geometry = result.geometry;
    //       let material;
    //
    //       if (result.materials !== undefined) {
    //
    //         if (result.materials.length > 1) {
    //
    //           material = new THREE.MultiMaterial(result.materials);
    //
    //         } else {
    //
    //           material = result.materials[ 0 ];
    //
    //         }
    //
    //       } else {
    //
    //         material = new THREE.MeshStandardMaterial();
    //
    //       }
    //
    //       geometry.sourceType = "ascii";
    //       geometry.sourceFile = file.name;
    //
    //       var mesh;
    //
    //       if (geometry.animation && geometry.animation.hierarchy) {
    //
    //         mesh = new THREE.SkinnedMesh(geometry, material);
    //
    //       } else {
    //
    //         mesh = new THREE.Mesh(geometry, material);
    //
    //       }
    //
    //       mesh.name = filename;
    //
    //       editor.execute(editor.commander.addObjectCommand.init(mesh));
    //
    //       break;
    //
    //     case 'object':
    //
    //       var loader = new THREE.ObjectLoader();
    //       loader.setTexturePath(scope.texturePath);
    //
    //       var result = loader.parse(data);
    //
    //       if (result instanceof THREE.Scene) {
    //
    //         editor.execute(editor.commander.setSceneCommand.init(result));
    //
    //       } else {
    //
    //         editor.execute(editor.commander.addObjectCommand.init(result));
    //
    //       }
    //
    //       break;
    //
    //     case 'app':
    //
    //       editor.fromJSON(data);
    //
    //       break;
    //
    //   }
    // }

    function createFileMap(files) {

      var map = {};

      for (var i = 0; i < files.length; i ++) {

        let file = files[ i ];
        map[ file.name ] = file;

      }

      return map;

    }

    function handleZIP(contents) {

      let zip = new JSZip(contents);

      if (zip.files[ 'model.obj' ] && zip.files[ 'materials.mtl' ]) {

        var materials = new THREE.MTLLoader().parse(zip.file('materials.mtl').asText());
        var object = new THREE.OBJLoader().setMaterials(materials).parse(zip.file('model.obj').asText());

        editor.execute(editor.commander.addObjectCommand.init(object));

      }

      zip.filter(function (path, file) {

        var manager = new THREE.LoadingManager();
        manager.setURLModifier(function (url) {

          var file = zip.files[ url ];

          if (file) {

            console.log('Loading', url);

            var blob = new Blob([ file.asArrayBuffer() ], { type: 'application/octet-stream' });
            return URL.createObjectURL(blob);

          }

          return url;

        });

        var extension = file.name.split('.').pop().toLowerCase();

        switch (extension) {

          case 'fbx':

            var loader = new FBXLoader(manager);
            var object = loader.parse(file.asArrayBuffer());

            object.traverse(function(child) {
              if (child.type == 'Mesh') {
                child.geometry = new THREE.Geometry().fromBufferGeometry(child.geometry);
              }
            });

            editor.execute(editor.commander.addObjectCommand.init(object));

            break;

          case 'glb':

            var loader = new THREE.GLTFLoader();

            loader.parse(file.asArrayBuffer(), '', function (result) {
              editor.execute(editor.commander.addObjectCommand.init(result.scene));
            });

            break;

          case 'gltf':

            var loader = new THREE.GLTFLoader(manager);

            loader.parse(file.asText(), '', function (result) {
              editor.execute(editor.commander.addObjectCommand.init(result.scene));
            });

            break;

        }
      });

    }

    function isGLTF1(contents) {

      var resultContent;

      if (typeof contents === 'string') {
        resultContent = contents;
      } else {

        var magic = THREE.LoaderUtils.decodeText(new Uint8Array(contents, 0, 4));

        if (magic === 'glTF') {
          var version = new DataView(contents).getUint32(4, true);
          return version < 2;
        } else {
          resultContent = THREE.LoaderUtils.decodeText(new Uint8Array(contents));
        }
      }

      var json = JSON.parse(resultContent);

      return (json.asset != undefined && json.asset.version[ 0 ] < 2);

    }
  }
}
