// /*
//  * Author: Max Schnaider / @maxschnaider
//  * Copyright (c) 2019.
//  */
//
// import Config from '~/config'
// import axios from 'axios'
// import SHA256 from 'crypto-js/sha256'
// import HMAC_SHA256 from 'crypto-js/hmac-sha256'
//
// export default class S3Service {
//
//   constructor (ctx, cognito) {
//
//     let canonicalRequest = 'POST\n' +
//       'http://s3.amazonaws.com/' + Config.awsBucket + '\n' +
//       '\n' +
//       'host:' + Config.awsBucket + '.s3.amazonaws.com\n' +
//       'x-amz-content-sha256:' + SHA256('').toString() + '\n' +
//       'x-amz-date:20190205T000000Z\n' +
//       'host;x-amz-content-sha256;x-amz-date\n' +
//       SHA256('').toString();
//
//
//     let stringToSign = "AWS4-HMAC-SHA256" + "\n" +
//       '20190205T000000Z' + "\n" +
//       '20190205/' + Config.awsRegion + '/s3/aws4_request' + "\n" +
//       SHA256(canonicalRequest).toString();
//
//     this.getSignatureKey = function (key, dateStamp, regionName, serviceName) {
//       let dateKey = HMAC_SHA256(dateStamp, "AWS4" + key);
//       let dateRegionKey = HMAC_SHA256(regionName, dateKey);
//       let dateRegionServiceKey = HMAC_SHA256(serviceName, dateRegionKey);
//       let signingKey = HMAC_SHA256("aws4_request", dateRegionServiceKey);
//       return signingKey;
//     };
//
//     let signingKey = this.getSignatureKey(Config.awsSecretAccessKey, '20190205T000000Z', Config.awsRegion, 's3');
//
//     let signature = HMAC_SHA256(stringToSign, signingKey).toString();
//
//     this.signature = signature;
//
//
//     this.fetchSignatureAndPolicy = function () {
//
//
//       let policy = '{ "expiration": "2019-02-05T12:00:00.000Z",' + '\n' +
//         '"conditions": [' + '\n' +
//         '{"bucket": "' + Config.awsBucket + '"},' + '\n' +
//         '["starts-with", "$key", "images/"],' + '\n' +
//         '{"acl": "public-read"},' + '\n' +
//         '{"success_action_redirect": "https://' + Config.awsBucket + '.s3.amazonaws.com/successful_upload.html"},' + '\n' +
//         '["starts-with", "$Content-Type", "image/jpeg"],' + '\n' +
//         '{"x-amz-meta-uuid": "14365123651274"},' + '\n' +
//         '["starts-with", "$x-amz-meta-tag", ""],' + '\n' +
//         '{"x-amz-credential": "' + Config.awsAccessKeyId + '/20190205/' + Config.awsRegion + '/s3/aws4_request"},' + '\n' +
//         '{"x-amz-algorithm": "AWS4-HMAC-SHA256"},' + '\n' +
//         '{"x-amz-date": "20190205T000000Z" }' + '\n' +
//         ']' + '\n' +
//         '}';
//
//       // let stringToSign1 = '{ "expiration": "2007-12-01T12:00:00.000Z",' + '\n' +
//       //   '"conditions": [' + '\n' +
//       //   '{"bucket": "johnsmith"},' + '\n' +
//       //   '["starts-with", "$key", "user/eric/"],' + '\n' +
//       //   '{"acl": "public-read"},' + '\n' +
//       //   '{"success_action_redirect": "http://johnsmith.s3.amazonaws.com/successful_upload.html"},' + '\n' +
//       //   '["starts-with", "$Content-Type", "image/"],' + '\n' +
//       //   '{"x-amz-meta-uuid": "14365123651274"},' + '\n' +
//       //   '["starts-with", "$x-amz-meta-tag", ""]' + '\n' +
//       //   ']' + '\n' +
//       //   '}';
//
//       let stringToSign = Buffer.from(policy).toString('base64');
//       let signingKey = this.getSignatureKey(Config.awsSecretAccessKey, '20190205T000000Z', Config.awsRegion, 's3');
//
//       let signature = HMAC_SHA256(stringToSign, signingKey).toString();
//       this.authHeader = 'AWS4-HMAC-SHA256 Credential=' + Config.awsAccessKeyId + '/20190205/' + Config.awsRegion + '/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=' + signature;
//
//
//       // console.log(signature);
//
//       return [signature, stringToSign];
//
//       // let url = 'https://' + Config.awsBucket + '.s3.amazonaws.com/';
//       //
//       // let reqParams = {
//       //   method: 'POST',
//       //   url: url,
//       //   data: JSON.stringify({
//       //
//       //   }),
//       //   headers: {
//       //     'Content-Type': 'multipart/form-data'
//       //   },
//       //   json: true
//       // };
//
//       // let res = await axios(reqParams);
//       //
//       // console.log(res);
//       //
//     }
//
//
//   }
//
// }
