/*
 * Project: artrend-frontend
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import UsersService from './api/UsersService'
import ProjectsService from './api/ProjectsService'
import TeamsService from './api/TeamsService'
import ModelsService from './api/ModelsService'
import SurfacesService from './api/SurfacesService'
import MaterialsService from './api/MaterialsService'
import MaterialCategoriesService from './api/MaterialCategoriesService'
import ScenesService from './api/ScenesService'
import EnvironmentsService from './api/EnvironmentsService'
import TrendsService from './api/TrendsService'

export default class ApiService {

  constructor(ctx) {
    this.users = new UsersService(ctx);
    this.projects = new ProjectsService(ctx);
    this.teams = new TeamsService(ctx);
    this.models = new ModelsService(ctx);
    this.surfaces = new SurfacesService(ctx);
    this.materials = new MaterialsService(ctx);
    this.materialCategories = new MaterialCategoriesService(ctx);
    this.scenes = new ScenesService(ctx);
    this.environments = new EnvironmentsService(ctx);
    this.trends = new TrendsService(ctx);
  }

}
