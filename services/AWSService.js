/*
 * Author: Max Schnaider / @maxschnaider
 * Copyright (c) 2019.
 */

import Config from '~/config'

// import CognitoService from './aws/CognitoService'
// import S3Service from './aws/S3Service'

export default class AWSService {

  constructor (ctx) {

    // this.cognito = new CognitoService(ctx);
    // this.s3 = new S3Service(ctx, this.cognito);
    // this.s3.uploadFile();

  }

}

// import Config from '~/config'
// const S3 = require('aws-sdk/clients/s3');
// const aws = require('aws-sdk/global');
// // // import { Auth, Storage } from 'aws-amplify'
// // import Storage from '@aws-amplify/storage'
// // import Auth from '@aws-amplify/auth'
//
// export default class AWSService {
//
//   constructor (ctx) {
//
//     // Auth.configure({
//     //   identityPoolId: Config.awsRegion + ':' + Config.awsIdentityPoolId,
//     //   region: Config.awsRegion
//     // });
//     //
//     // Storage.configure({
//     //   bucket: Config.awsBucket,
//     //   region: Config.awsRegion
//     // });
//     //
//     // Storage.put('test2.txt', 'Hello')
//     //   .then (result => console.log(result))
//     //   .catch(err => console.log(err));
//
//     aws.config.update({
//       region: Config.awsRegion,
//       credentials: new aws.CognitoIdentityCredentials({
//         IdentityPoolId: Config.awsRegion + ':' + Config.awsIdentityPoolId
//       })
//     });
//
//     let s3 = new S3({
//       apiVersion: '2006-03-01',
//       // params: {Bucket: Config.awsBucket}
//     });
//
//
//     s3.listObjects({
//       Bucket: Config.awsBucket
//     }, function(err, data) {
//       if (err) {
//         console.log('ERROR: ' + err);
//       } else {
//         console.log(data);
//       }
//     });
//
//
//
//   }
//
// }
